import("stdfaust.lib");

pitchRangeLow = 80;
pitchRangeHi = 440;
coRangeLow = 50;
coRangeHi = 10000;

//UI
waveSelect = hslider("wave_select", 0, 0, 2, 1);
pitch = hslider("pitch", 440, pitchRangeLow, pitchRangeHi, 1) : si.smoo;
egPitch = hslider("eg_pitch", 0, 0, 1, .01) : si.smoo;
filterMode = hslider("filter_select", 0, 0, 2, 1) : si.smoo;
cutoff = hslider("cutoff", 500, coRangeLow, coRangeHi, 0.01) : si.smoo;
q = hslider("q", 5, 1, 30, 0.1) : si.smoo;
egFilter = hslider("eg_filter", 0, 0, 1, .01) : si.smoo;
a = hslider("attack", 0, 0, .5, .1) : si.smoo;
d = hslider("decay", .1, 0, 2, .1) : si.smoo;
gain = hslider("gain", 0.5, 0, 1, 0.01) : si.smoo;
fm_amount = hslider("fm_amount", 0, 0, 5000, 1) : si.smoo;
trigger = ba.beat(120); // button("trigger");

// STREAM AUDIO
ar = en.ar(a, d, trigger);

computedPitch = ((pitchRangeHi - pitchRangeLow) * (ar * egPitch) + pitchRangeLow) + pitch;
computedCutoff = ((coRangeHi - coRangeLow) * (ar * egFilter) + coRangeLow) + cutoff;

oscModulator = os.osc(computedPitch);

oscSaw = os.sawtooth(computedPitch+oscModulator*fm_amount);
oscPulse = os.pulsetrain(computedPitch+oscModulator*fm_amount, 0.5);
oscNoise = no.noise;

osc = oscSaw, oscPulse, oscNoise : ba.selectn(3, waveSelect);

lpFilter = fi.resonlp(computedCutoff, q, 0.9);
hpFilter = fi.resonhp(computedCutoff, q, 0.9);

filter = lpFilter, hpFilter : ba.selectn(2, filterMode);

process = osc <: filter * ar * gain;
