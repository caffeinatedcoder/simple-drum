#include <Audio.h>

// GUItool: begin automatically generated code
AudioSynthWaveformSine   sine1;          //xy=136,227
AudioAmplifier           amp1;           //xy=296,311
AudioOutputAnalog        dac1;           //xy=451,225
AudioConnection          patchCord1(sine1, amp1);
AudioConnection          patchCord2(amp1, dac1);
// GUItool: end automatically generated code

bool play;
int pitch = 0;

void setup() {
  Serial.begin(9600);

  AudioMemory(8);
  AudioInterrupts();
  dac1.analogReference(INTERNAL);

  play = true;

  sine1.amplitude(1.0);
  sine1.frequency(440);
  amp1.gain(1.0);

  Serial.println("setup ok!");
}

void loop() {

  if (play) {
    Serial.println("play");
    sine1.frequency(440);
    amp1.gain(0.3);
  } else {
    Serial.println("silence");
    amp1.gain(0);
  }

  play = !play;

  Serial.print("CPU:");
  Serial.print(AudioProcessorUsage());
  Serial.print(" CPU_MAX:");
  Serial.print(AudioProcessorUsageMax());
  Serial.print(" MEM:");
  Serial.print(AudioMemoryUsage());
  Serial.print(" MEM_MAX:");
  Serial.print(AudioMemoryUsageMax());
  Serial.println();

  delay(1000);
}
