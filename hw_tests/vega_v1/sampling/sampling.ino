#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#define LED_ENC_A_PIN 36

// GUItool: begin automatically generated code
AudioInputAnalog         adc1;           //xy=151,473
AudioAmplifier           amp1;           //xy=292,475
AudioAnalyzePeak         peak1;          //xy=442,610
AudioRecordQueue         queue1;         //xy=444,563
AudioOutputAnalog        dac1;           //xy=445,473
AudioConnection          patchCord1(adc1, amp1);
AudioConnection          patchCord2(amp1, dac1);
AudioConnection          patchCord3(amp1, queue1);
AudioConnection          patchCord4(adc1, peak1);
// GUItool: end automatically generated code

uint32_t FreeMem(){ // for Teensy 3.0
  uint32_t stackTop;
  uint32_t heapTop;

  // current position of the stack.
  stackTop = (uint32_t) &stackTop;

  // current position of heap.
  void* hTop = malloc(1);
  heapTop = (uint32_t) hTop;
  free(hTop);

  // The difference is (approximately) the free, available ram.
  return stackTop - heapTop;
}


void setup() {
  Serial.begin(9600);

  AudioMemory(30);
  AudioInterrupts();
  dac1.analogReference(INTERNAL);

  amp1.gain(0.05);

  pinMode(LED_ENC_A_PIN, OUTPUT);

  Serial.println("setup ok!");
  Serial.println(FreeMem() / 1048576.0, 2);

}

void loop() {
  analogWrite(LED_ENC_A_PIN, map(peak1.read(), 0.0, 1.0, 0, 255));
  delay(128);
}
