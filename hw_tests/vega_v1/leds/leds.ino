#define LED_BTN_A_PIN 5
#define LED_BTN_B_PIN 2
#define LED_BTN_C_PIN 3
#define LED_BTN_D_PIN 4
#define LED_BTN_E_PIN 6

#define LED_MODE_A_PIN 21
#define LED_MODE_B_PIN 23
#define LED_MODE_C_PIN 30
#define LED_MODE_D_PIN 29

#define LED_ENC_A_PIN 36
#define LED_ENC_B_PIN 35

void setup() {
  Serial.begin(9600);

  pinMode(LED_BTN_A_PIN, OUTPUT);
	pinMode(LED_BTN_B_PIN, OUTPUT);
  pinMode(LED_BTN_C_PIN, OUTPUT);
  pinMode(LED_BTN_D_PIN, OUTPUT);
  pinMode(LED_BTN_E_PIN, OUTPUT);

  pinMode(LED_MODE_A_PIN, OUTPUT);
  pinMode(LED_MODE_B_PIN, OUTPUT);
  pinMode(LED_MODE_C_PIN, OUTPUT);
  pinMode(LED_MODE_D_PIN, OUTPUT);

  pinMode(LED_ENC_A_PIN, OUTPUT);
  pinMode(LED_ENC_B_PIN, OUTPUT);

  Serial.println("setup ok!");
}

void loop() {

  digitalWrite(LED_BTN_A_PIN, HIGH);
	digitalWrite(LED_BTN_B_PIN, HIGH);
  digitalWrite(LED_BTN_C_PIN, HIGH);
  digitalWrite(LED_BTN_D_PIN, HIGH);
  digitalWrite(LED_BTN_E_PIN, HIGH);

  digitalWrite(LED_MODE_A_PIN, HIGH);
  digitalWrite(LED_MODE_B_PIN, HIGH);
  digitalWrite(LED_MODE_C_PIN, HIGH);
  digitalWrite(LED_MODE_D_PIN, HIGH);

  digitalWrite(LED_ENC_A_PIN, HIGH);
  digitalWrite(LED_ENC_B_PIN, HIGH);

  delay(30);
}
