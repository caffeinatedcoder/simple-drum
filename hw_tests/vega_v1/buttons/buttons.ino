#define BUTTON_A_PIN 27
#define BUTTON_B_PIN 26
#define BUTTON_C_PIN 25
#define BUTTON_D_PIN 24
#define BUTTON_E_PIN 39

#define ENCODER_A_BUTTON_PIN 38
#define ENCODER_B_BUTTON_PIN 37

#define BUTTONS_LENGTH 7

#include <Bounce2.h>

Bounce *buttons[BUTTONS_LENGTH];

void setup() {
  Serial.begin(9600);

  pinMode(BUTTON_A_PIN, INPUT_PULLUP);
	pinMode(BUTTON_B_PIN, INPUT_PULLUP);
	pinMode(BUTTON_C_PIN, INPUT_PULLUP);
	pinMode(BUTTON_D_PIN, INPUT_PULLUP);
	pinMode(BUTTON_E_PIN, INPUT_PULLUP);
  pinMode(ENCODER_A_BUTTON_PIN, INPUT_PULLUP);
  pinMode(ENCODER_B_BUTTON_PIN, INPUT_PULLUP);

  for (int i=0; i<BUTTONS_LENGTH; i++) {
		buttons[i] = new Bounce();
	}

	buttons[0]->attach(BUTTON_A_PIN);
	buttons[1]->attach(BUTTON_B_PIN);
	buttons[2]->attach(BUTTON_C_PIN);
	buttons[3]->attach(BUTTON_D_PIN);
  buttons[4]->attach(BUTTON_E_PIN);
  buttons[5]->attach(ENCODER_A_BUTTON_PIN);
  buttons[6]->attach(ENCODER_B_BUTTON_PIN);

  Serial.println("setup ok!");
}

void loop() {
  for (int i=0; i<BUTTONS_LENGTH; i++) {
    buttons[i]->update();
  }

  for (int i=0; i<BUTTONS_LENGTH; i++) {
		if (buttons[i]->fell()) {
      Serial.println(i);
    }
	}

  delay(30);
}
