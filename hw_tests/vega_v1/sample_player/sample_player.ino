#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>

#define SDCARD_CS_PIN    BUILTIN_SDCARD
//#define SDCARD_MOSI_PIN  11  // not actually used
//#define SDCARD_SCK_PIN   13  // not actually used

// GUItool: begin automatically generated code
AudioPlaySdWav           playMem1;       //xy=109,47
AudioAmplifier           amp1;           //xy=250,43
AudioOutputAnalog        dac1;           //xy=406,43
AudioConnection          patchCord1(playMem1, 0, amp1, 0);
AudioConnection          patchCord2(amp1, dac1);
// GUItool: end automatically generated code

bool play;
const char *filename = "SN1.WAV";

void setup() {
  Serial.begin(9600);

  AudioMemory(8);
  AudioInterrupts();
  dac1.analogReference(INTERNAL);

  play = true;

  amp1.gain(0.3);

  Serial.println("setup ok!");

  //SPI.setMOSI(SDCARD_MOSI_PIN);
  //SPI.setSCK(SDCARD_SCK_PIN);

  boolean status = SD.begin(SDCARD_CS_PIN);
  if (status) {
    Serial.println("SD library is able to access the filesystem");
  } else {
    Serial.println("SD library can not access the filesystem!");
  }


  // Open the 4 sample files.  Hopefully they're on the card
  File f1 = SD.open(filename);

  if (f1) {
    Serial.print(filename);
    Serial.println(" EXISTS!");
  }
}

void loop() {

  if (play) {
    playMem1.play(filename);
  }

  play = !play;

  Serial.print("CPU:");
  Serial.print(AudioProcessorUsage());
  Serial.print(" CPU_MAX:");
  Serial.print(AudioProcessorUsageMax());
  Serial.print(" MEM:");
  Serial.print(AudioMemoryUsage());
  Serial.print(" MEM_MAX:");
  Serial.print(AudioMemoryUsageMax());
  Serial.println();

  delay(1000);
}
