#ifndef __VEGA_DESK_DEFINES__
#define __VEGA_DESK_DEFINES__

#include <Arduino.h>

#define VEGA
#define DEBUG_AUDIO_RESOURCES

typedef void (*voidFuncPtr)(void);

// #define PI 3.14159265358979323846

#define EMA_ALPHA 0.2
#define MIN_ANALOG_VALUE 0
#define MAX_ANALOG_VALUE 1024

#define ONE_SECOND 60000
#define A440 440

// GENERAL
#define FPS 1000/30 //ms 30fps

// BUTTONS
#define BUTTON_A_PIN 0
#define BUTTON_B_PIN 1
#define BUTTON_C_PIN 2
#define BUTTON_D_PIN 3
#define BUTTON_E_PIN 4

// CV/GATE PINS
#define GATE_A_IN_PIN 23
#define GATE_B_IN_PIN 22
#define GATE_C_IN_PIN 21
#define GATE_D_IN_PIN 20
#define GATE_E_IN_PIN 10

// LEDS
#define LED_BTN_A_PIN 5
#define LED_BTN_B_PIN 6
#define LED_BTN_C_PIN 7
#define LED_BTN_D_PIN 8

#define LED_ENC_A_PIN 9
#define LED_ENC_B_PIN 12

// ANALOG POTS
#define ANALOG_POT_PIN_A 15
#define ANALOG_POT_PIN_B 14

// SD CARD
#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11
#define SDCARD_SCK_PIN   13

#endif
