#include "src/ui/pot.h"
#include "defines.h"

Pot *potA = new Pot();
Pot *potB = new Pot();

void setup() {
  Serial.begin(9600);
  potA->Setup(ANALOG_POT_PIN_A, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
  potB->Setup(ANALOG_POT_PIN_B, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
}

void loop() {
  int a = potA->ReadAs64();
  int b = potB->ReadAs64();

  Serial.print("pota:");
  Serial.print(a);
  Serial.print(" potb:");
  Serial.print(b);
  Serial.println();

  delay(30);
}
