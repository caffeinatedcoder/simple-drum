int tic;
int ticReset;
int maxVoltage;

void setup() {
  Serial.begin(9800);
  tic = 0;
  ticReset = 100;
  maxVoltage = 4000;
}

void loop() {
  // SAW UP
  int sawUp = maxVoltage * (tic) / ticReset;

  // SAW DOWN
  int sawDown = maxVoltage * (ticReset - (tic)) / ticReset;

  // TRIANGLE
  int triangle = maxVoltage * (abs(tic - ticReset * 0.5) * 2) / ticReset;

  int sine = maxVoltage * ((ticReset * 0.5 * sin(tic / 16.0)) + (ticReset * 0.5)) / ticReset;

  // INCREMENT TIC
  tic = (tic + 1) % ticReset;

  //SERIAL PLOTTER
  Serial.print("MAX:");
  Serial.print(maxVoltage);
  Serial.print(" SAWUP:");
  Serial.print(sawUp);
  Serial.print(" SAWDOWN:");
  Serial.print(sawDown);
  Serial.print(" TRIANGLE:");
  Serial.print(triangle);
  Serial.print(" TRIANGLE:");
  Serial.print(triangle);
  Serial.print(" SINE:");
  Serial.print(sine);
  Serial.print(" ");
  Serial.print(maxVoltage/2);
  Serial.println();

  //FREQUENZA: 30FPS
  delay(1000/30);
}
