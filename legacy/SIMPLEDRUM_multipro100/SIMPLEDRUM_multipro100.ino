
/*  Teensy 3.2 & 3.6 MiniDrum
 *  Author: FaseLunare
 * Portions taken from the Teensy "Examples" and from
 * https://github.com/UECIDE/Teensy3/tree/master/cores/teensy3/files/libraries/Audio/examples/SamplePlayer
 * 
 * 
 * 
 */
 
#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <Bounce.h>

// Button input pins on Teensy 3.2 & 3.6

#define sound0Pin 0
#define sound1Pin 1
#define sound2Pin 2
#define sound3Pin 3
#define funct0Pin 4


// Tr input pins on Teensy 3.2 & 3.6

#define sound0Trig 23  
#define sound1Trig 22
#define sound2Trig 21
#define sound3Trig 20

// RND input pins on Teensy 3.2 & 3.6

#define rnd0Pin 10  

//  Led pin 

#define sound0Led   5
#define sound1Led   6
#define sound2Led   7 
#define sound3Led   8
#define func0Led    9
#define rnd0Led    12


//  Pot analog pin
#define pot1       14 // as A1 for input mode in PINMODE
#define pot2       15 // as A0 for input mode in PINMODE


// WAV files converted to code by wav2sketch
#include "AudioSampleSnare.h"        // http://www.freesound.org/people/KEVOY/sounds/82583/
#include "AudioSampleTomtom.h"       // http://www.freesound.org/people/zgump/sounds/86334/
#include "AudioSampleHihat.h"        // http://www.freesound.org/people/mhc/sounds/102790/
#include "AudioSampleKick.h"         // http://www.freesound.org/people/DWSD/sounds/171104/


// Create the Audio components.  

AudioPlayMemory    sound0;
AudioPlayMemory    sound1;  // four memory players
AudioPlayMemory    sound2;  // for converted wav samples
AudioPlayMemory    sound3;
AudioMixer4        mix1;    // two 4-channel mixers are needed in
AudioMixer4        mix2;    // tandem to combine 6 audio sources
AudioOutputAnalog  dac;     // play on-chip DAC

// Internal synth generated sounds copied from Teensy example audio sketch
AudioSynthSimpleDrum     drum0;          
AudioSynthSimpleDrum     drum1;          
AudioMixer4              mixer1;  // 4 channel mixer to combine sounds and connect to other mixer input

// Create Audio connections between the components
AudioConnection c1(sound0, 0, mix1, 0);
AudioConnection c2(sound1, 0, mix1, 1);
AudioConnection c3(sound2, 0, mix1, 2);
AudioConnection c4(sound3, 0, mix1, 3);
AudioConnection c5(mix1, 0, mix2, 0);   // output of mix1 into 1st input on mix2
AudioConnection c10(mix2, 0, dac, 0);
AudioConnection c11(mixer1, 0, mix2, 3);  // tie in the internal synth mixer output to sample sounds mixer
AudioConnection patchCord1(drum0, 0, mixer1, 0);  
AudioConnection patchCord2(drum1, 0, mixer1, 1);


// Bounce objects to read four pushbuttons,four trigger & RND input 

Bounce button0 = Bounce(sound0Pin, 5);
Bounce button1 = Bounce(sound1Pin, 5);  // 5 ms debounce time
Bounce button2 = Bounce(sound2Pin, 5);
Bounce button3 = Bounce(sound3Pin, 5);
Bounce button4 = Bounce(funct0Pin, 5);

Bounce Trig0 =   Bounce(sound0Trig, 2);
Bounce Trig1 =   Bounce(sound1Trig, 2);  // 2 ms debounce time
Bounce Trig2 =   Bounce(sound2Trig, 2);
Bounce Trig3 =   Bounce(sound3Trig, 2);
Bounce TrigRnd = Bounce(rnd0Pin, 2);



void setup() {
  // Configure the pushbutton pins for pullups.
  // Each button should connect from the pin to GND.

  
  
  pinMode(sound0Pin, INPUT_PULLUP);
  pinMode(sound1Pin, INPUT_PULLUP);
  pinMode(sound2Pin, INPUT_PULLUP);
  pinMode(sound3Pin, INPUT_PULLUP);
  pinMode(funct0Pin, INPUT_PULLUP);


  // Configure the trigger pins for pullups.

  pinMode(sound0Trig, INPUT_PULLUP);
  pinMode(sound1Trig, INPUT_PULLUP);
  pinMode(sound2Trig, INPUT_PULLUP);
  pinMode(sound3Trig, INPUT_PULLUP);
  pinMode(rnd0Pin,    INPUT_PULLUP);
  pinMode(rnd0Pin,    INPUT_PULLUP);
  pinMode(A0, INPUT);
  pinMode(A1, INPUT);

  // Configure the LED OUT
 

  pinMode(sound0Led, OUTPUT);
  pinMode(sound1Led, OUTPUT);
  pinMode(sound2Led, OUTPUT);
  pinMode(sound3Led, OUTPUT);
  pinMode(func0Led,  OUTPUT);
  pinMode(rnd0Led,   OUTPUT);

  


  // open serial port for Terminal

  Serial.begin(115200); 
    delay(400);
    Serial.println("Teensy 3.2 & 3.6 MiniDrum");
    delay(300);
    Serial.println("Author: FaseLunare");


  
  
  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  AudioMemory(25);

  // turn on the output
  //audioShield.enable();
  //audioShield.volume(0.5);

  // by default the Teensy 3.1 DAC uses 3.3Vp-p output
  // if your 3.3V power has noise, switching to the
  // internal 1.2V reference can give you a clean signal
  dac.analogReference(INTERNAL);

  // reduce the gain on mixer channels, so more than 1
  // sound can play simultaneously without clipping
  mix1.gain(0, 0.4);
  mix1.gain(1, 0.4);
  mix1.gain(2, 0.4);
  mix1.gain(3, 0.4);
  mix2.gain(1, 0.4);
  mix2.gain(2, 0.4);
  mixer1.gain(0, 0.4);
  mixer1.gain(1, 0.4);
   
  // configure what the synth drums will sound like
  //drum0.frequency(60);
  //drum0.length(300);
  //drum0.secondMix(0.0);
  //drum0.pitchMod(1.0);
  
  //drum1.frequency(60);
  //drum1.length(1500);
  //drum1.secondMix(0.0);
  //drum1.pitchMod(0.55);

  //initial led show

  digitalWrite(sound0Led, HIGH);
  delay(200);
  digitalWrite(sound0Led, LOW);
  delay(100);
  digitalWrite(sound1Led, HIGH);
  delay(200);
  digitalWrite(sound1Led, LOW);
  delay(100);
  digitalWrite(sound2Led, HIGH);
  delay(200);
  digitalWrite(sound2Led, LOW);
  delay(100);
  digitalWrite(sound3Led, HIGH);
  delay(200);
  digitalWrite(sound3Led, LOW);
  delay(100);
  digitalWrite(func0Led, HIGH);
  digitalWrite(rnd0Led,  HIGH);
  delay(100);
  digitalWrite(func0Led, LOW);
  digitalWrite(rnd0Led,  LOW);
  delay(100);
  digitalWrite(func0Led, HIGH);
  digitalWrite(rnd0Led,  HIGH);
  delay(100);
  digitalWrite(func0Led, LOW);
  digitalWrite(rnd0Led,  LOW);
  
  

  AudioInterrupts();
}

void loop() {
  
  // Update all the button objects
  
  button0.update();
  button1.update();
  button2.update();
  button3.update();
  button4.update();
  

  // Update all the Trigger objects

  Trig0.update();
  Trig1.update();
  Trig2.update();
  Trig3.update();
  TrigRnd.update();

  // Make sure that LEDs are OFF
  
  digitalWrite(sound0Led, LOW);
  digitalWrite(sound1Led, LOW);
  digitalWrite(sound2Led, LOW);
  digitalWrite(sound3Led, LOW);
  digitalWrite(func0Led,  LOW);

  

  
  // When the buttons are pressed, just start a sound playing.
  // The audio library will play each sound through the mixers
  // so any combination can play simultaneously.
  //
  
    if (button0.fallingEdge()) {
    sound3.play(AudioSampleKick);
    Serial.println("Kick");
    digitalWrite(sound0Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound0Led, LOW);
    
   }
  
  if (button1.fallingEdge()) {
    sound0.play(AudioSampleSnare);
    Serial.println("Snare");
    digitalWrite(sound1Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound1Led, LOW);
  }
  if (button2.fallingEdge()) {
    sound2.play(AudioSampleHihat);
    Serial.println("HiHat");
    digitalWrite(sound2Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound2Led, LOW);
}
  if (button3.fallingEdge()) {
    sound1.play(AudioSampleTomtom);
    Serial.println("TomTom");
    digitalWrite(sound3Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound3Led, LOW);
  }

  // When the Trigger goes HIGH ( in hardware, goes LOW in this sketch)
  // just start a sound playing.
  // The audio library will play each sound through the mixers
  // so any combination can play simultaneously.
  //
  
    if (Trig0.fallingEdge()) {
    sound3.play(AudioSampleKick);
    Serial.println("Kick");
    digitalWrite(sound0Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound0Led, LOW);
   }
  
  if (Trig1.fallingEdge()) {
    sound0.play(AudioSampleSnare);
    Serial.println("Snare");
    digitalWrite(sound1Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound1Led, LOW);
  }
  if (Trig2.fallingEdge()) {
    sound2.play(AudioSampleHihat);
     Serial.println("HiHat");
    digitalWrite(sound2Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound2Led, LOW);
}
  if (Trig3.fallingEdge()) {
    sound1.play(AudioSampleTomtom);
    Serial.println("TomTom");
    digitalWrite(sound3Led, HIGH);  // LED on
    delay(100);
    digitalWrite(sound3Led, LOW);
  }

  }
 
