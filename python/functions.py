import matplotlib.pyplot as plt
import math

def func_inverse_linear(x, w, h):
	return h - (x * h / w);

def func_log(x, beta, w, h):
	return beta * (-1. + math.exp( (float)((w-x) / w) * math.log(1. + (h / beta))) )

def func_circle(i, radius, numPoints):
	theta = 2 * math.pi * i / numPoints
	xpos = radius * math.sin(theta)
	ypos = radius * math.cos(theta)
	return [xpos, ypos]

def func_arc(i, radius, angle, rotation):
	theta = 2 * math.pi * i / 360
	xpos = radius * math.sin(theta)
	ypos = radius * math.cos(theta)
	return rotate_point(xpos, ypos, rotation)

def rotate_point(x, y, angle):
	theta = 2 * math.pi * angle / 360
	return [x*math.sin(theta) - y*math.cos(theta), y*math.sin(theta) + x*math.cos(theta)]

def arc_length(radius, angle):
	return (math.pi*(radius*2)) * (angle/360)

def draw_arc(radius, arcAngle):
	points_x = []
	point_y = []
	for i in range(arcAngle):
		[xpos, ypos] = func_arc(i, radius, arcAngle, -45)
		points_x.append(xpos)
		point_y.append(ypos)
	return [points_x, point_y]

def draw_linear_ticks(radius, arcAngle, ticksNum):
	points_x = []
	point_y = []

	for i in range(ticksNum + 1):
		iTick = math.ceil(i * arcAngle / ticksNum);
		[xpos, ypos] = func_arc(iTick, radius, arcAngle, -45)
		points_x.append(xpos)
		point_y.append(ypos)
	return [points_x, point_y]

def draw_ticks(radius, arcAngle, ticksNum):
	points_x = []
	point_y = []

	for i in range(ticksNum + 1):
		iTick = func_log(i, 1, ticksNum, 360)
		[xpos, ypos] = func_arc(iTick, radius, arcAngle, -45)
		points_x.append(xpos)
		point_y.append(ypos)
	return [points_x, point_y]

arcAngle = 270

[a1x, a1y] = draw_arc(230, arcAngle)
[a2x, a2y] = draw_arc(210, arcAngle)
[t1x, t1y] = draw_ticks(240, arcAngle, 10)
[t2x, t2y] = draw_ticks(220, arcAngle, 20)

plt.plot(a1x, a1y)
plt.plot(a2x, a2y)
plt.scatter(t1x, t1y, 4)
plt.scatter(t2x, t2y, 4)
plt.show()
