import matplotlib.pyplot as plt
import math

def func_arc(i, radius, angle, rotation):
	theta = 2 * math.pi * i / 360
	xpos = radius * math.sin(theta)
	ypos = radius * math.cos(theta)
	return rotate_point(xpos, ypos, rotation)

def draw_arc(radius, arcAngle, rotation):
	points_x = []
	point_y = []
	for i in range(arcAngle):
		[xpos, ypos] = func_arc(i, radius, arcAngle, rotation)
		points_x.append(xpos)
		point_y.append(ypos)
	return [points_x, point_y]

def func_log(x, beta, w, h):
	return beta * (-1. + math.exp( (float)((w-x) / w) * math.log(1. + (h / beta))) )

def func_arc(i, radius, angle, rotation):
	theta = 2 * math.pi * i / 360
	xpos = radius * math.cos(theta)
	ypos = radius * math.sin(theta)
	return rotate_point(xpos, ypos, rotation)

def rotate_point(x, y, angle):
	theta = 2 * math.pi * angle / 360
	return [x*math.cos(theta) - y*math.sin(theta), y*math.cos(theta) + x*math.sin(theta)]

def draw_line_mark(x, y, radius, mark_angle, mark_length):
	#deg to rad
	theta = 2 * math.pi * mark_angle / 360
	x1 = x + radius * math.cos(theta)
	y1 = y + radius * math.sin(theta)
	x2 = x + (radius + mark_length) * math.cos(theta)
	y2 = y + (radius + mark_length) * math.sin(theta)

	#line = inkex.PathElement()
	#line.path = 'M {},{} L {},{}'.format(x1, y1, x2, y2) 

	return [x1, y1, x2, y2]

def draw_ticks(radius, angle, n_ticks, ticks_start_angle, mode):
	points_x = []
	point_y = []

	for tick in range(n_ticks):

		if mode == 'lin':
			ticks_delta = ticks_start_angle + (angle / (n_ticks - 1)) * tick
		elif mode == 'log':
			ticks_delta = ticks_start_angle + func_log(tick, 1, n_ticks, angle)

		[x1, y1, x2, y2] = draw_line_mark(0, 0, radius, ticks_delta, n_ticks)
		points_x.append(x1)
		point_y.append(y1)
		print(tick, ticks_delta, x1, y1)

	return [points_x, point_y]

def draw_test(radius, arcAngle, ticks, rotation, mode): 
	[a1x, a1y] = draw_arc(radius, arcAngle, rotation)
	plt.plot(a1x, a1y)
	[t1x, t1y] = draw_ticks(radius, arcAngle, ticks, rotation, mode)
	plt.scatter(t1x, t1y, 20)

draw_test(100, 270, 10, -45, 'lin')
draw_test(90, 270, 10, -45, 'log')

plt.show()


# -------------------------------------------

def func_log_test(x, beta, w, h):
	# math log function
	# it calculate the y value for x
	# beta is the curve squashing factor
	# w is the max x value
	# h is the max y value 
	return beta * (-1. + math.exp( (float)((w-x) / w) * math.log(1. + (h / beta))) )

def draw_line_mark_test(x, y, radius, mark_angle, mark_length):
	# given an index 'mark_angle' on a line it calculates the position around a circle
	# x, y: shift from (0,0)
	# radius: radius of the circle
	# mark_angle: position on a line (0 to 360, as the position is the angle on the circle)
	# mark_length: max number of positions

	# deg to rad
	theta = 2 * math.pi * mark_angle / 360
	# you spin me round and round like a baby
	x1 = x + radius * math.cos(theta)
	y1 = y + radius * math.sin(theta)
	x2 = x + (radius + mark_length) * math.cos(theta)
	y2 = y + (radius + mark_length) * math.sin(theta)

	# draw stuffs
	line = inkex.PathElement()
	line.path = 'M {},{} L {},{}'.format(x1, y1, x2, y2)

	return [x1, y1, x2, y2]

def draw_ticks_test(radius, angle, n_ticks, ticks_start_angle, mode):
	# cycle through the ticks and draw points along an arc
	# radius: radius of the circle
	# angle: angle of the arc (deg)
	# n_ticks: number of segments 
	# ticks_start_angle: angle offset (deg)
	# mode: lin -> linear distribution / log -> logaritmic distribution

	for tick in range(n_ticks):

		# calculate index based on the function and add the offset angle
		if mode == 'lin':
			ticks_delta = ticks_start_angle + (angle / (n_ticks - 1)) * tick
		elif mode == 'log':
			ticks_delta = ticks_start_angle + func_log_test(tick, 1, n_ticks, angle)

		# draw stuffs and get the points returned for logging
		[x1, y1, x2, y2] = draw_line_mark_test(0, 0, radius, ticks_delta, n_ticks)

		# check the output
		print(tick, ticks_delta, x1, y1, x2, y2)

# -------------------------------------------
