# Fase Lunare || Simple Drum

Four Part Drum Machine made on Teensy. (Yeah, you got it!)

## How to run the code

(TBD)

## Project folder and main .ino

The project directory is dev/simple_drum/, while the main .ino file is simple_drum.ino

## Other Directories

- /assets: images and other display related stuffs
- /docs: contains hardware documentation, such as schematics
- /legacy: legacy code

## Project Structure

(TBD)

## Requirements

- Audio
- Wire
- SPI
- SD
- Bounce

## Audio Signal Path

	+-------+
	|       |
	|  BD   +--+
	|       |  |
	+-------+  |
	           |
	+-------+  |
	|       |  |
	|  SD   +--+
	|       |  |  +-------+    +-------+    +-------+    +-------+
	+-------+  |  |       |    |       |    |       |    |       |
	           +->+  MIX  +--->+  FX1  +--->+  FX2  +--->+  DAC  |
	+-------+  |  |       |    |       |    |       |    |       |
	|       |  |  +-------+    +-------+    +-------+    +-------+
	|  HH   +--+
	|       |  |
	+-------+  |
	           |
	+-------+  |
	|       |  |
	|  PC   +--+
	|       |
	+-------+

## Synthesis

	BASS DRUM

	+-------+   +-----+
	| SINE  +-->+ ENV |
	+-------+   +-----+

	SNARE DRUM

	+-------+   +--------+   +-----+
	| WHITE +-->+ BP FLT +-->+ ENV |
	+-------+   +--------+   +-----+

	HIHAT

	+-------+   +--------+   +-----+
	| WHITE +-->+ HP FLT +-->+ ENV |
	+-------+   +--------+   +-----+

	PERC

	+------+
	| PWM1 +---+
	+------+   |
	           |
	+------+   |   +-------+   +--------+   +-----+
	| PWM2 +------>+ MIXER +-->+ LP FLT +-->+ ENV |
	+------+   |   +-------+   +--------+   +-----+
	           |
	+-------+  |
	| NOISE +--+
	+-------+

## Functions

	+-------+--------------+--------------+--------------+
	|       | MODE 0       | MODE 1       | MODE 2       |
	+=======+==============+==============+==============+
	| Pot A | release time | attack time  | FX depth     |
	+-------+--------------+--------------+--------------+
	| Pot B | pitch/cutoff | volume       | FX Param     |
	+-------+--------------+--------------+--------------+
	| Btn 0 | select BD                   | select FX1   |
	+-------+--------------+--------------+--------------+
	| Btn 1 | select SD                   | select FX2   |
	+-------+--------------+--------------+--------------+
	| Btn 2 | select HH                   | store        |
	+-------+--------------+--------------+--------------+
	| Btn 3 | select PC                   | recall       |
	+-------+--------------+--------------+--------------+

## Credits

Board developed and assembled By Francesco Mulassano and Alessandro Comanzo
Software developed by Daniele Pagliero.

## License

(TBD)
