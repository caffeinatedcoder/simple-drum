#include <Audio.h>
#include "src/synth/_include.h"
#include "src/engine.h"
#include "src/programs/_include.h"

Engine *engine = Engine::Instance();

void setup() {
  AudioMemory(40);
  AudioInterrupts();

  Synth *synth = new Synth();
  synth->Init();

  engine->Setup();
  engine->GetProgramSwitcher()->PushProgram(new ProgramStartup());
  engine->GetProgramSwitcher()->PushProgram(new MainProgram(synth));
}

void loop() {
  engine->Loop();
}
