#include "utils.h"

int exponential_moving_average(int value, int last) {
  return (EMA_ALPHA*value) + ((1-EMA_ALPHA)*last);
}

int bpm_to_time(int bpm, int numerator, int denominator) {
  float division = (numerator*1.)/denominator;
	return static_cast<int>(ONE_SECOND/bpm/(1./4./division));
}

float m_to_f(int note) {
  //return /*static_cast<int>*/(A440 / 32.) * (2 ^ ((note - 9) / 12));
  return 440.*(pow(2., ((note-69.)/12.)));
}

float func_linear(int x, int w, int h) {
  // JS
  // return x * h / w;
  return x * h / w;
}

float func_inverse_linear(int x, int w, int h) {
  // JS
  // return h - (x * h / w);
  return h - (x * h / w);
}

float func_exp(int x, int beta, int w, int h) {
  // JS
  // return h - (beta * (-1 + Math.exp( (x / w) * Math.log(1 + (h / beta))) ));
  return h - (beta * (-1 + exp( (x / w) * log(1 + (h / beta))) ));
}

float func_log(int x, int beta, int w, int h) {
  // JS
  // return beta * (-1 + Math.exp( ((w-x) / w) * Math.log(1 + (h / beta))) );
  return beta * (-1 + exp( ((w-x) / w) * log(1 + (h / beta))) );
}

float func_slope(int x, int step, int r, int w, int h) {
  // JS
  // return h - (h * (0.5 + ( (Math.atan( (x - step) / r ) ) / Math.PI )));
  return h - (h * (0.5 + ( (atan( (x - step) / r ) ) / PI )));
}
