#ifndef __FASE_LUNARE_CORE_LED__
#define __FASE_LUNARE_CORE_LED__

#include <Arduino.h>

class Led {
public:
  Led();
  void Setup(int pin);
  void Reset();
  void Set(bool value);
  void Show();
private:
  int _pin;
  bool _value;
};

#endif
