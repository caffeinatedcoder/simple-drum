#ifndef __FASE_LUNARE_CORE_PROGRAM__
#define __FASE_LUNARE_CORE_PROGRAM__

#include <Arduino.h>

class Program {
public:
  Program();
  virtual void Setup();
  virtual void Loop();
  void SetNext(Program *program);
  Program *GetNext();
private:
  Program *_next;
};

#endif
