#ifndef __FASE_LUNARE_CORE_PROGRAM_SWITCHER__
#define __FASE_LUNARE_CORE_PROGRAM_SWITCHER__

#include <Arduino.h>
#include "program.h"

class ProgramSwitcher {
public:
  ProgramSwitcher();
  void PushProgram(Program *program);
  void NextProgram();
  void PrevProgram();
  void GoToProgram(int index);
  Program *GetCurrentProgram();
  int CountPrograms();
  int CurrentProgramIndex();
private:
  Program *getNth(int index);

  Program *_root;
  int _current;
};

#endif
