#include "engine.h"

Engine* Engine::_instance = NULL;

Engine* Engine::Instance() {
	if(_instance == NULL){
		_instance = new Engine();
	}
	return _instance;
}

Engine::Engine() {
	_mainTimer = new Timer();

	_potA = new Pot();
	_potB = new Pot();

	for (int i=0; i<LED_COUNT; i++) {
		_leds[i] = new Led();
	}

	for (int i=0; i<BUTTONS_COUNT; i++) {
		_buttons[i] = new Bounce();
	}

	for (int i=0; i<GATES_COUNT; i++) {
		_gatesIn[i] = new Bounce();
	}

	_programSwitcher = new ProgramSwitcher();

	// initialize private members
	_state = false;
}

Engine::~Engine() {
	if(_instance != 0) delete _instance;
}

void Engine::Setup() {
	//board IO
	_mainTimer->Setup();

  _potA->Setup(ANALOG_POT_PIN_A, false);
  _potB->Setup(ANALOG_POT_PIN_B, false);

	_leds[0]->Setup(LED_PIN_BD);
	_leds[1]->Setup(LED_PIN_SD);
	_leds[2]->Setup(LED_PIN_HH);
	_leds[3]->Setup(LED_PIN_PC);
	_leds[4]->Setup(LED_PIN_A);
	_leds[5]->Setup(LED_PIN_B);

	pinMode(BUTTON_PIN_A, INPUT_PULLUP);
	pinMode(BUTTON_PIN_B, INPUT_PULLUP);
	pinMode(BUTTON_PIN_C, INPUT_PULLUP);
	pinMode(BUTTON_PIN_D, INPUT_PULLUP);
	pinMode(BUTTON_PIN_FUNC, INPUT_PULLUP);

	_buttons[0]->attach(BUTTON_PIN_A);
	_buttons[1]->attach(BUTTON_PIN_B);
	_buttons[2]->attach(BUTTON_PIN_C);
	_buttons[3]->attach(BUTTON_PIN_D);
	_buttons[4]->attach(BUTTON_PIN_FUNC);

	_gatesIn[0]->attach(TRIGGER_PIN_A);
	_gatesIn[1]->attach(TRIGGER_PIN_B);
	_gatesIn[2]->attach(TRIGGER_PIN_C);
	_gatesIn[3]->attach(TRIGGER_PIN_D);
	_gatesIn[4]->attach(TRIGGER_PIN_RND);
}

void Engine::Loop() {
  unsigned long elapsed = _mainTimer->GetElapsed();

  if (elapsed >= FPS) {
    _mainTimer->Update();

		for (int i=0; i<BUTTONS_COUNT; i++) {
			_buttons[i]->update();
		}

		for (int i=0; i<GATES_COUNT; i++) {
			_gatesIn[i]->update();
		}

		for (int i=0; i<LED_COUNT; i++) {
			_leds[i]->Reset();
		}

		// execute program
		if (_programSwitcher->GetCurrentProgram() != NULL) {
			_programSwitcher->GetCurrentProgram()->Loop();
		}

		for (int i=0; i<6; i++) {
			_leds[i]->Show();
		}

		_state = !_state;
  }
}

// GETTERS
ProgramSwitcher *Engine::GetProgramSwitcher() {
	return _programSwitcher;
}

Pot *Engine::GetPotA() {
	return _potA;
}

Pot *Engine::GetPotB() {
	return _potB;
}

Led *Engine::GetLed(int index) {
	return _leds[index % LED_COUNT];
}

Bounce *Engine::GetButton(int num) {
	return _buttons[num % BUTTONS_COUNT];
}

Bounce *Engine::GetGateIn(int num) {
	return _gatesIn[num % GATES_COUNT];
}
