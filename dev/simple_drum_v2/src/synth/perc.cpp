#include "perc.h"

Perc::Perc(Ad *env, AudioMixer4 *out, int channel,
  AudioSynthWaveformPWM *pwm1, AudioSynthWaveformPWM *pwm2,
  AudioSynthNoisePink *noise, AudioMixer4 *mixer, AudioFilterBiquad *filter) : Part(env, out, channel) {
  _volume = 2;
  _frequency = 12;
  _attack = 0;
  _release = 5;

  _pwm1 = pwm1;
  _pwm2 = pwm2;
  _noise = noise;
  _mixer = mixer;
  _filter = filter;

  _pwm1->amplitude(0.5);
  _pwm2->amplitude(0.8);

  _noise->amplitude(0.5);
  _filter->setBandpass(0, 1200, 0.5);

  _mixer->gain(0, 0.4);
  _mixer->gain(1, 0.4);
  _mixer->gain(2, 0.2);

  Init();
}

void Perc::SetFrequency(int value) {
  _frequency = value;
  int f2 = _frequency + 12;
  _pwm1->frequency(m_to_f(map(_frequency, 0, 64, 24, 96)));
  _pwm2->frequency(m_to_f(map(f2, 0, 64, 24, 96)));
}
