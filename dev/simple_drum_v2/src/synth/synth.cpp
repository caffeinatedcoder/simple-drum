#include "synth.h"

// GUItool: begin automatically generated code
AudioSynthWaveformSine   sine1;          //xy=66,36
AudioSynthNoiseWhite     noise2;         //xy=67,156
AudioSynthNoiseWhite     noise1;         //xy=68,102
AudioSynthNoisePink      noise3;         //xy=68,329
AudioSynthWaveformPWM    pwm1;           //xy=69,224
AudioSynthWaveformPWM    pwm2;           //xy=71,276
AudioFilterBiquad        biquad2;        //xy=211,155
AudioFilterBiquad        biquad1;        //xy=212,102
AudioMixer4              mixer2;         //xy=218,282
Ad                       envelope1;      //xy=372,33 AudioEffectEnvelope
Ad                       envelope4;      //xy=372,282
Ad                       envelope2;      //xy=373,101
Ad                       envelope3;      //xy=373,155
AudioMixer4              mixer1;         //xy=552,136
AudioEffectBitcrusher    bitcrusher1;    //xy=685,104
AudioMixer4              mixer3;         //xy=821,141
AudioEffectGranular      granular1;      //xy=952,107
AudioMixer4              mixer4;         //xy=1084,147
AudioOutputAnalogStereo  dac1;           //xy=1214,147

AudioConnection          patchCord1(sine1, envelope1);
AudioConnection          patchCord2(noise2, biquad2);
AudioConnection          patchCord3(noise1, biquad1);
AudioConnection          patchCord4(noise3, 0, mixer2, 2);
AudioConnection          patchCord5(pwm1, 0, mixer2, 0);
AudioConnection          patchCord6(pwm2, 0, mixer2, 1);
AudioConnection          patchCord7(biquad2, envelope3);
AudioConnection          patchCord8(biquad1, envelope2);
AudioConnection          patchCord9(mixer2, envelope4);
AudioConnection          patchCord10(envelope1, 0, mixer1, 0);
AudioConnection          patchCord11(envelope4, 0, mixer1, 3);
AudioConnection          patchCord12(envelope2, 0, mixer1, 1);
AudioConnection          patchCord13(envelope3, 0, mixer1, 2);
AudioConnection          patchCord14(mixer1, bitcrusher1);
AudioConnection          patchCord15(mixer1, 0, mixer3, 1);
AudioConnection          patchCord16(bitcrusher1, 0, mixer3, 0);
AudioConnection          patchCord17(mixer3, granular1);
AudioConnection          patchCord18(mixer3, 0, mixer4, 1);
AudioConnection          patchCord19(granular1, 0, mixer4, 0);
AudioConnection          patchCord20(mixer4, 0, dac1, 0);
// GUItool: end automatically generated code

Synth::Synth() {

}

void Synth::Init() {
  dac1.analogReference(INTERNAL);

  //fx1
  mixer3.gain(0, 0);
  mixer3.gain(1, 0.5);

  //fx2
  mixer4.gain(0, 0);
  mixer4.gain(1, 0.5);

  sine1.frequency(110);
  envelope1.attack(15);
  envelope1.release(500);
  mixer1.gain(0, 0.5);

  _parts[0] = new Bd808(&envelope1, &mixer1, 0, &sine1);
  _parts[1] = new Sd808(&envelope2, &mixer1, 1, &noise1, &biquad1);
  _parts[2] = new Hh(&envelope3, &mixer1, 2, &noise2, &biquad2);
  _parts[3] = new Perc(&envelope4, &mixer1, 3, &pwm1, &pwm2, &noise3, &mixer2, &biquad2);
}

Part *Synth::GetPart(int num) {
  return _parts[num % 4];
}
