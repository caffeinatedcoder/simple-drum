#ifndef __SYNTH_BD__
#define __SYNTH_BD__

#include <Audio.h>
#include "part.h"

class Bd808 : public Part {
public:
  Bd808(Ad *env, AudioMixer4 *out, int channel, AudioSynthWaveformSine *osc);
  void SetFrequency(int value);
private:
  AudioSynthWaveformSine *_osc;
};

#endif
