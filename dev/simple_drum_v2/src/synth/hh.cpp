#include "hh.h"

/*

  +-------+   +--------+   +-----+
  | WHITE +-->+ HP FLT +-->+ ENV |
  +-------+   +--------+   +-----+

*/

Hh::Hh(Ad *env, AudioMixer4 *out, int channel, AudioSynthNoiseWhite *noise, AudioFilterBiquad *filter) : Part(env, out, channel) {
  _volume = 32;
  _frequency = 35;
  _attack = 0;
  _release = 3;

  _noise = noise;
  _filter = filter;

  _noise->amplitude(0.1);

  Init();
}

void Hh::SetFrequency(int value) {
  _frequency = value;
  int v = map(_frequency, 0, 64, 400, 6000);
  _filter->setHighpass(0, v, 0.2);
}
