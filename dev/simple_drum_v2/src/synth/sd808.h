#ifndef __SYNTH_SD808__
#define __SYNTH_SD808__

#include <Audio.h>
#include "part.h"

class Sd808 : public Part {
public:
  Sd808(Ad *env, AudioMixer4 *out, int channel, AudioSynthNoiseWhite *noise, AudioFilterBiquad *filter);
  void SetFrequency(int value);
private:
  AudioSynthNoiseWhite *_noise;
  AudioFilterBiquad *_filter;
};

#endif
