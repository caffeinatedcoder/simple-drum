#ifndef __SYNTH_PART__
#define __SYNTH_PART__

#include <Arduino.h>
#include <Audio.h>
#include "../audio/_include.h"
#include "../utils/_include.h"

class Part {
public:
  Part(Ad *env, AudioMixer4 *out, int channel);
  void Init();
  void Trigger();
  void SetAttack(int value);
  void SetRelease(int value);
  void SetVolume(int value);
  bool IsActive();
  int GetFrequency();
  int GetAttack();
  int GetRelease();
  int GetVolume();
  virtual void SetFrequency(int value);
protected:
  AudioMixer4 *_out;
  Ad *_env;
  int _channel;
  int _frequency;
  int _attack;
  int _release;
  int _volume;
};

#endif
