#include "bd808.h"

/*

  +-------+   +-----+
  | SINE  +-->+ ENV |
  +-------+   +-----+

*/

Bd808::Bd808(Ad *env, AudioMixer4 *out, int channel, AudioSynthWaveformSine *osc) : Part(env, out, channel) {
  _volume = 54;
  _frequency = 16;
  _attack = 0;
  _release = 25;

  _osc = osc;
  Init();
}

void Bd808::SetFrequency(int value) {
  _frequency = value;
  float v = m_to_f(map(_frequency, 0, 64, 24, 96));
  _osc->frequency(v);
}
