#ifndef __SYNTH_HH__
#define __SYNTH_HH__

#include <Audio.h>
#include "part.h"

class Hh : public Part {
public:
  Hh(Ad *env, AudioMixer4 *out, int channel, AudioSynthNoiseWhite *noise, AudioFilterBiquad *filter);
  void SetFrequency(int value);
private:
  AudioSynthNoiseWhite *_noise;
  AudioFilterBiquad *_filter;
};

#endif
