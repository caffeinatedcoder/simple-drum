#ifndef __SYNTH_CONFIG__
#define __SYNTH_CONFIG__

#include <Arduino.h>
#include <Audio.h>
#include "bd808.h"
#include "hh.h"
#include "perc.h"
#include "sd808.h"

void init_synth();
Part *init_part(int num);

class Synth {
public:
  Synth();
  void Init();
  Part *GetPart(int num);
private:
  Part *_parts[4];
};

#endif
