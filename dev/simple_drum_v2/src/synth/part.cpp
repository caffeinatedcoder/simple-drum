#include "part.h"

Part::Part(Ad *env, AudioMixer4 *out, int channel) {
  _volume = 32;
  _frequency = 32;
  _attack = 1;
  _release = 32;

  _env = env;
  _out = out;
  _channel = channel;
}

void Part::Init() {
  SetAttack(_attack);
  SetRelease(_release);
  SetFrequency(_frequency);
  SetVolume(_volume);
}

void Part::Trigger() {
  _env->noteOn();
}

void Part::SetAttack(int value) {
  _attack = value;
  int v = map(value, 0, 64, 10, 2000);
  _env->attack(v);
}

void Part::SetRelease(int value) {
  _release = value;
  int v = map(value, 0, 64, 10, 2000);
  _env->release(v);
}

void Part::SetVolume(int value) {
  _volume = value;
  _out->gain(_channel, _volume / 64.0);
}

bool Part::IsActive() {
  return _env->isActive();
}

void Part::SetFrequency(int value) {
  //virtual method
}

int Part::GetFrequency() {
  return _frequency;
}

int Part::GetAttack() {
  return _attack;
}

int Part::GetRelease() {
  return _release;
}

int Part::GetVolume() {
  return _volume;
}
