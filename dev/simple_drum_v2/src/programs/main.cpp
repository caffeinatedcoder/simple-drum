#include "main.h"

MainProgram::MainProgram(Synth *synth) : Program() {
  _synth = synth;
  Setup();
}

void MainProgram::Setup() {
  _timer = new Timer();
  _cycle = 0;

  // selected part
  _selectedIndex = 0;
  _selectedFx = 0;

  // matrix params
  _mode = 0;

  // params lock
  _aLock = true;
  _bLock = true;
  _aMem = getSelectedAParam();
  _bMem = getSelectedBParam();

  // system
  _cycle = 0;
  _blink = true;
}

void MainProgram::Loop() {
  Engine *engine = Engine::Instance();

  int leds[] = {0,0,0,0};

  processTriggers(leds);
  processButtons();
  updateParams();
  updateLeds(leds);

  _cycle = (_cycle + 1) % 64;
  _blink = (_cycle % 4);
}

void MainProgram::processTriggers(int *leds) {
  /*Engine *engine = Engine::Instance();

  for (int i=0; i<4; i++) {
    if (engine->GetGateIn(i)->fallingEdge()) {
      _parts[i]->Trigger();
    }
  }*/

  // return;

  //simulate triggers

  int bd[] = {0, 1};
  int sd[] = {4, 7};
  int hh[] = {2, 6};
  int perc[] = {3, 5};

  int division = 8;

  for (uint i = 0; i < sizeof(bd)/sizeof(bd[0]); i++) {
    if (_cycle == bd[i]*division) {
      _synth->GetPart(0)->Trigger();
      leds[0] = 1;
    }
  }

  for (uint i = 0; i < sizeof(sd)/sizeof(sd[0]); i++) {
    if (_cycle == sd[i]*division) {
      _synth->GetPart(1)->Trigger();
      leds[1] = 1;
    }
  }

  for (uint i = 0; i < sizeof(hh)/sizeof(hh[0]); i++) {
    if (_cycle == hh[i]*division) {
      _synth->GetPart(2)->Trigger();
      leds[2] = 1;
    }
  }

  for (uint i = 0; i < sizeof(perc)/sizeof(perc[0]); i++) {
    if (_cycle == perc[i]*division) {
      _synth->GetPart(3)->Trigger();
      leds[3] = 1;
    }
  }
}

void MainProgram::processButtons() {
  Engine *engine = Engine::Instance();

  bool changed = false;

  if (_mode == 0 || _mode == 1) {

    for (int i=0; i < 4; i++) {
      if (engine->GetButton(i)->fell()) {
        _selectedIndex = i;
        changed = true;
        break;
      }
    }

  } else if (_mode == 2) {

    for (int i=0; i < 2; i++) {
      if (engine->GetButton(i)->fell()) {
        _selectedFx = i;
        changed = true;
        break;
      }
    }

    if (engine->GetButton(2)->fell()) {
      // TODO: STORE
      /*Serial.println("-----BEGIN-----");

      for (int i=0; i<4; i++) {

        Serial.println(_parts[i]->GetAttack());
        Serial.println(_parts[i]->GetRelease());
        Serial.println(_parts[i]->GetFrequency());
        Serial.println(_parts[i]->GetVolume());
        Serial.println("----------");
      }

      Serial.println("----------");
      Serial.println(_fx1Depth);
      Serial.println(_fx1Param);
      Serial.println("----------");
      Serial.println(_fx2Depth);
      Serial.println(_fx2Param);
      Serial.println("-----END-----");*/
    }

    if (engine->GetButton(3)->fell()) {
    }
  }

  if (engine->GetButton(4)->fell()) {
    _mode = (_mode + 1) % 3;
    changed = true;
  }

  if (changed) {
    _aMem = getSelectedAParam();
    _bMem = getSelectedBParam();
    _aLock = true;
    _bLock = true;
  }
}

void MainProgram::updateParams() {
  Engine *engine = Engine::Instance();

  int a = engine->GetPotA()->ReadAs64();
  int b = engine->GetPotB()->ReadAs64();

  if (a == _aMem) {
    engine->GetLed(4)->Set(true);
    _aLock = false;
  }

  if (b == _bMem) {
    engine->GetLed(5)->Set(true);
    _bLock = false;
  }

  if (!_aLock) {
    setSelectedAParam(a);
  }

  if (!_bLock) {
    setSelectedBParam(b);
  }
}

void MainProgram::updateLeds(int *leds) {
  Engine *engine = Engine::Instance();

  if (_mode == 0 || _mode == 1) {
    for (int i = 0; i < 4; i++) {
      if (i != _selectedIndex) {
        engine->GetLed(i)->Set(leds[i]);
      } else {
        engine->GetLed(i)->Set(!leds[i]);
      }
    }
  } else {
    engine->GetLed(_selectedFx)->Set(_blink);
  }

  //TODO: mode led not present on the board yet
}

int MainProgram::getSelectedAParam() {
  if (_mode == 0) {
    return _synth->GetPart(_selectedIndex)->GetFrequency();
  } else if (_mode == 1) {
    return _synth->GetPart(_selectedIndex)->GetVolume();
  } else if (_mode == 2) {
    //return _selectedFx==0 ? _fx1Depth : _fx2Depth;
  }

  return -1;
}

int MainProgram::getSelectedBParam() {
  if (_mode == 0) {
    return _synth->GetPart(_selectedIndex)->GetRelease();
  } else if (_mode == 1) {
    return _synth->GetPart(_selectedIndex)->GetAttack();
  } else if (_mode == 2) {
    //return _selectedFx==0 ? _fx1Param : _fx2Param;
  }

  return -1;
}

void MainProgram::setSelectedAParam(int value) {
  if (_mode == 0) {
    _synth->GetPart(_selectedIndex)->SetFrequency(value);
  } else if (_mode == 1) {
    _synth->GetPart(_selectedIndex)->SetVolume(value);
  } else if (_mode == 2) {
    /*if (_selectedFx == 0) {
      _fx1Depth = value;
      setFxDepth(_fx1Mix, _fx1Depth);
    } else {
      _fx2Depth = value;
      setFxDepth(_fx2Mix, _fx2Depth);
    }*/
  }
}

void MainProgram::setSelectedBParam(int value) {
  if (_mode == 0) {
    _synth->GetPart(_selectedIndex)->SetRelease(value);
  } else if (_mode == 1) {
    _synth->GetPart(_selectedIndex)->SetAttack(value);
  } else if (_mode == 2) {
    /*if (_selectedFx == 0) {
      _fx1Param = value;
      setFxParam(_fx1Param);
    } else {
      _fx2Param = value;
      setFxParam(_fx2Param);
    }*/
  }
}
