#ifndef __PROGRAM_MAIN__
#define __PROGRAM_MAIN__

#include <Arduino.h>
#include "../engine.h"
#include "../synth/_include.h"
#include "../utils/_include.h"

class MainProgram : public Program {
public:
  MainProgram(Synth *synth);
  void Setup();
  void Loop();
private:
  Synth *_synth;
  Timer *_timer;

  // selected part
  int _selectedIndex;
  int _selectedFx;

  // matrix params
  int _mode;

  // params lock
  bool _aLock;
  bool _bLock;
  int _aMem;
  int _bMem;

  // system
  int _cycle;
  bool _blink;

  void processTriggers(int *leds);
  void processButtons();
  void updateParams();
  void updateLeds(int *leds);

  int getSelectedAParam();
  void setSelectedAParam(int value);

  int getSelectedBParam();
  void setSelectedBParam(int value);
};

#endif
