#ifndef __FASE_LUNARE_ENGINE__
#define __FASE_LUNARE_ENGINE__

#include <Arduino.h>
#include <Bounce2.h>

#include "../defines.h"
#include "core/_include.h"
#include "ui/_include.h"

class Engine {
public:
  //singleton pattern
  static Engine *Instance();
  //end singleton pattern

  void Setup();
  void Loop();

  // getters
  ProgramSwitcher   *GetProgramSwitcher();
  Pot               *GetPotA();
  Pot               *GetPotB();
  Led               *GetLed(int index);
  Bounce            *GetButton(int num);
  Bounce            *GetGateIn(int num);
private:
  // singleton pattern
  static Engine* _instance;
  Engine();
  ~Engine();
  Engine(const Engine&);
  Engine& operator=(const Engine&);
  // end singleton pattern

  bool              _state;
  Timer             *_mainTimer;
  // board ui
  Pot               *_potA;
  Pot               *_potB;
  Led               *_leds[LED_COUNT];
  Bounce            *_buttons[BUTTONS_COUNT];
  Bounce            *_gatesIn[GATES_COUNT];

  ProgramSwitcher   *_programSwitcher;
};

#endif
