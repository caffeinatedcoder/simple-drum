#ifndef __FASE_LUNARE_CONSTANTS__
#define __FASE_LUNARE_CONSTANTS__

// #define PI 3.14159265358979323846

#define EMA_ALPHA 0.2
#define MAX_ANALOG_VALUE 1010 //830 //854

#define ONE_SECOND 60000
#define A440 440

// GENERAL
#define FPS 1000/30 //ms 30fps

#define LED_COUNT 6
#define BUTTONS_COUNT 5
#define GATES_COUNT 5

// BUTTONS
#define BUTTON_PIN_A 0
#define BUTTON_PIN_B 1
#define BUTTON_PIN_C 2
#define BUTTON_PIN_D 3
#define BUTTON_PIN_FUNC 4

// CV TRIGGERS
#define TRIGGER_PIN_A 23
#define TRIGGER_PIN_B 22
#define TRIGGER_PIN_C 21
#define TRIGGER_PIN_D 20
#define TRIGGER_PIN_RND 10

// LEDs
#define LED_PIN_BD 5
#define LED_PIN_SD 6
#define LED_PIN_HH 7
#define LED_PIN_PC 8
#define LED_PIN_A 9
#define LED_PIN_B 12

// ANALOG POTS
#define ANALOG_POT_PIN_A 15
#define ANALOG_POT_PIN_B 14

#endif
