#include <Audio.h>
#include "src/synth/_include.h"
#include "src/engine.h"
#include "src/programs/_include.h"

#include "src/utils/_include.h"

Engine *engine = Engine::Instance();

void setup() {

#ifdef DEBUG_AUDIO_RESOURCES
  Serial.begin(9600);
#endif

  SD.begin(SDCARD_CS_PIN);

  AudioMemory(24);
  AudioInterrupts();

  Synth *synth = new Synth();
  synth->Init();

  engine->Setup();
  engine->programSwitcher->PushProgram(new ProgramStartup());
  engine->programSwitcher->PushProgram(new MainProgram(synth));
}

void loop() {
  engine->Loop();
}

/*#include <Audio.h>
#include "src/VegaVoice/VegaVoice.h"
#include "src/pot.h"
#include "defines.h"

int coRangeLow = 50;
int coRangeHi = 10000;

AudioOutputAnalog dac;
VegaVoice vegaVoice;
AudioConnection patchCord0(faustProgram,0,dac,0);

Pot potA;
Pot potB;

void setup() {
  //Serial.begin(9600);
  AudioMemory(20);

  potA.Setup(ANALOG_POT_PIN_A, false);
  potB.Setup(ANALOG_POT_PIN_B, false);
}

void loop() {
  int cutoff = potA.ReadAs64();

  //Serial.print("cutoff:");
  //Serial.print(cutoff);
  //Serial.println();

  vegaVoice.setParamValue("cutoff", map(cutoff, 0, 64, coRangeLow, coRangeHi));

}*/
