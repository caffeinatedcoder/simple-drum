#ifndef __FASE_LUNARE_CORE_LED__
#define __FASE_LUNARE_CORE_LED__

#include <Arduino.h>

class Led {
public:
  Led();
  void Setup(int pin, bool isDigital = false);
  void Reset();
  void Set(bool value);
  void Set(int value);
  void Show();
private:
  int _pin;
  int _analogValue;
  bool _isDigital;
  bool _initialized;
};

#endif
