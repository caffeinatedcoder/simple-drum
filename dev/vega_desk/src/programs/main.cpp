#include "main.h"

MainProgram::MainProgram(Synth *synth) : Program() {
  _synth = synth;
  Setup();
}

void MainProgram::Setup() {
  _timer = new Timer();
  _cycle = 0;

  // selected voice
  _selectedIndex = 0;
  _selectedFx = 0;

  // matrix params
  _page = 0;

  // params lock
  _aLock = true;
  _bLock = true;
  _aMem = getSelectedAParam();
  _bMem = getSelectedBParam();

  // system
  _cycle = 0;
  _blink = true;

  for (int i=0; i<4; i++) {
    _ledStates[i] = false;
  }
}

void MainProgram::Run() {
}

void MainProgram::Loop() {
  //processTriggers();
  //processButtons();
  updateParams();
  updateLeds();

  _cycle = (_cycle + 1) % 64;
  _blink = (_cycle % 4);

  #ifdef DEBUG_AUDIO_RESOURCES
  /*
    Serial.print("CPU:");
    Serial.print(AudioProcessorUsage());
    Serial.print(" CPU_MAX:");
    Serial.print(AudioProcessorUsageMax());
    Serial.print(" MEM:");
    Serial.print(AudioMemoryUsage());
    Serial.print(" MEM_MAX:");
    Serial.print(AudioMemoryUsageMax());
    Serial.println();*/
  #endif
}

void MainProgram::processTriggers() {
  //simulate triggers

  int bd[] = {0, 1};
  int sd[] = {4, 7};
  int hh[] = {2, 6};
  int perc[] = {3, 5};

  int division = 8;

  for (uint i = 0; i < sizeof(bd)/sizeof(bd[0]); i++) {
    if (_cycle == bd[i]*division) {
      OnGateIn(0);
    }
  }

  for (uint i = 0; i < sizeof(sd)/sizeof(sd[0]); i++) {
    if (_cycle == sd[i]*division) {
      OnGateIn(1);
    }
  }

  for (uint i = 0; i < sizeof(hh)/sizeof(hh[0]); i++) {
    if (_cycle == hh[i]*division) {
      OnGateIn(2);
    }
  }

  for (uint i = 0; i < sizeof(perc)/sizeof(perc[0]); i++) {
    if (_cycle == perc[i]*division) {
      OnGateIn(3);
    }
  }
}

void MainProgram::OnKeyDown(int key) {
  Serial.print("key down:");
  Serial.print(key);
  Serial.println();

  bool changed = false;

  if (key == 4) {
    _page = (_page + 1) % 6;
    changed = true;
  } else {
    if (_page < 5) {
      _selectedIndex = key;
      changed = true;
    } else {
      if (key < 2) {
        _selectedFx = key;
        changed = true;
      }
    }
  }

  if (changed) {
    _aMem = getSelectedAParam();
    _bMem = getSelectedBParam();
    _aLock = true;
    _bLock = true;
  }
}

void MainProgram::processButtons() {
  /*Engine *engine = Engine::Instance();

  bool changed = false;

  if (_page < 5) {

    for (int i=0; i < 4; i++) {
      if (engine->buttons[i]->fell()) {
        _selectedIndex = i;
        changed = true;
        break;
      }
    }

  } else {

    for (int i=0; i < 2; i++) {
      if (engine->buttons[i]->fell()) {
        _selectedFx = i;
        changed = true;
        break;
      }
    }

    if (engine->buttons[2]->fell()) {
      // TODO: STORE
      _synth->EEpromStore();
    }

    if (engine->buttons[3]->fell()) {
      _synth->EEpromLoad();
    }
  }

  if (engine->buttons[4]->fell()) {
    _page = (_page + 1) % 6;
    changed = true;
  }

  if (changed) {
    _aMem = getSelectedAParam();
    _bMem = getSelectedBParam();
    _aLock = true;
    _bLock = true;
  }*/
}

void MainProgram::updateParams() {
  Engine *engine = Engine::Instance();

  int a = engine->potA->ReadAs64();
  int b = engine->potB->ReadAs64();

  if (a == _aMem) {
    engine->encoderLeds[0]->Set(true);
    _aLock = false;
  }

  if (b == _bMem) {
    engine->encoderLeds[1]->Set(true);
    _bLock = false;
  }

  if (!_aLock) {
    setSelectedAParam(a);
  }

  if (!_bLock) {
    setSelectedBParam(b);
  }
}

void MainProgram::updateLeds() {
  Engine *engine = Engine::Instance();

  if (_page < 5) {
    for (int i = 0; i < BUTTONS_LEDS - 1; i++) {
      if (i != _selectedIndex) {
        engine->buttonLeds[i]->Set(_ledStates[i]);
      } else {
        engine->buttonLeds[i]->Set(!_ledStates[i]);
      }
    }

    /*if (_page == 4) {
      engine->modeLeds[0]->Set(32);
      engine->modeLeds[1]->Set(32);
    } else {
      engine->modeLeds[_page]->Set(32);
    }*/

  } else {
    engine->buttonLeds[_selectedFx]->Set(_blink);
    // engine->buttonLeds[_blink + 2]->Set(1);
    /*for (int i = 0; i < 4; i++) {
      engine->modeLeds[i]->Set(32);
    }*/
  }

  for (int i=0; i<4; i++) {
    _ledStates[i] = false;
  }
}

int MainProgram::getSelectedAParam() {
  if (_synth->GetVoice(_selectedIndex) == NULL) {
    return -1;
  }

  if (_page < 5) {
    return _synth->GetVoice(_selectedIndex)->GetParameter(matrix[_page * 2]);
  } else {
    return _synth->GetFxDepth(_selectedFx);
  }

  return -1;
}

int MainProgram::getSelectedBParam() {
  if (_page < 5) {
    if (_synth->GetVoice(_selectedIndex) == NULL) {
      return -1;
    }

    return _synth->GetVoice(_selectedIndex)->GetParameter(matrix[_page * 2 + 1]);
  } else {
    return _synth->GetFxParam(_selectedFx);
  }

  return -1;
}

void MainProgram::setSelectedAParam(int value) {
  if (_page < 5) {
    if (_synth->GetVoice(_selectedIndex) == NULL) {
      return;
    }
    _synth->GetVoice(_selectedIndex)->SetParameter(matrix[_page * 2], value);
  } else {
    _synth->SetFxDepth(_selectedFx, value);
  }
}

void MainProgram::setSelectedBParam(int value) {
  if (_page < 5) {
    if (_synth->GetVoice(_selectedIndex) == NULL) {
      return;
    }
    _synth->GetVoice(_selectedIndex)->SetParameter(matrix[_page * 2 + 1], value);
  } else {
    _synth->SetFxParam(_selectedFx, value);
  }
}

void MainProgram::OnGateIn(uint8_t channel) {
  int selected = channel;
  if (channel == 4) {
    selected = random(0, 4);
  }

  Voice *v = _synth->GetVoice(selected);

  if (v == NULL) {
    return;
  }

  v->Trigger();
  _ledStates[selected] = true;
}
