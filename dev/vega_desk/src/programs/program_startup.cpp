#include "program_startup.h"

ProgramStartup::ProgramStartup() {
  Setup();
}

void ProgramStartup::Setup() {
  _timer = new Timer();
  _tic = 0;
}

void ProgramStartup::Loop() {

  Engine *engine = Engine::Instance();

  engine->buttonLeds[0]->Set(_tic > 1 ? true : false);
  engine->buttonLeds[1]->Set(_tic > 2 ? true : false);
  engine->buttonLeds[2]->Set(_tic > 3 ? true : false);
  engine->buttonLeds[3]->Set(_tic > 4 ? true : false);

  engine->encoderLeds[0]->Set(_tic > 5 ? true : false);
  engine->encoderLeds[1]->Set(_tic > 6 ? true : false);

  if (_tic >= 7) {
    engine->programSwitcher->NextProgram();
    _tic = 0;
  }

  _tic ++;
}
