#ifndef __SUB_ORBITAL_ENGINE__
#define __SUB_ORBITAL_ENGINE__

#include "../defines.h"
#ifdef VEGA

#include <Audio.h>
#include <Arduino.h>
#include <Bounce2.h>

#include "./core/_include.h"
#include "./io/_include.h"
#include "./ui/_include.h"
#include "./utils/_include.h"

const int BUTTONS_LEDS = 4;
const int ENCODER_LEDS = 2;
const int BUTTONS = 5;
const int GATES = 5;


class Engine {
public:
  //singleton pattern
  static Engine *Instance();
  //end singleton pattern

  void Setup();
  void Loop();
  void OnKeyDown();

  ProgramSwitcher *programSwitcher;

  //board
  Led *buttonLeds[BUTTONS_LEDS];
  // Led *modeLeds[4];
  Led *encoderLeds[ENCODER_LEDS];

  Button *buttons[BUTTONS];
  // Encoder *encoderA;
  // Encoder *encoderB;

  Gate *gates[GATES];

  Pot *potA;
  Pot *potB;

private:
  // singleton pattern
  static Engine* _instance;
  Engine();
  ~Engine();
  Engine(const Engine&);
  Engine& operator=(const Engine&);
  // end singleton pattern

  static void gateInAInterruptCallback();
  static void gateInBInterruptCallback();
  static void gateInCInterruptCallback();
  static void gateInDInterruptCallback();
  static void gateInEInterruptCallback();

  static void buttonAInterruptCallback();
  static void buttonBInterruptCallback();
  static void buttonCInterruptCallback();
  static void buttonDInterruptCallback();
  static void buttonEInterruptCallback();

  bool _state;
  Timer *_mainTimer;
};

#endif
#endif
