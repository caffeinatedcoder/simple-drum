#include "synth_drum.h"

const char *paramsLabels[VOICE_PARAMS_LENGTH] = {
  "pitch",
  "co",
  "res",
  "feg",
  "peg",
  "a",
  "d",
  "wave",
  "vol",
  "mode",
  "in"
};

SynthDrum::SynthDrum(Ad *env, AudioMixer4 *out, int channel,
  AudioSynthWaveformModulated *osc,
  AudioSynthNoiseWhite *noise,
  AudioPlaySdWav *smpl,
  AudioMixer4 *oscMix,
  AudioMixer4 *filterMix,
  AudioFilterStateVariable *filter,
  AudioAmplifier *fegLevel,
  AudioAmplifier *pegLevel
  ) : Voice(env, out, channel) {

  _params[VOICE_PARAM_PITCH] = 20;
  _params[VOICE_PARAM_CUTOFF] = 64;
  _params[VOICE_PARAM_RESONANCE] = 36;
  _params[VOICE_PARAM_ATTACK] = 0;
  _params[VOICE_PARAM_DECAY] = 36;
  _params[VOICE_PARAM_VOLUME] = 64;
  _params[VOICE_PARAM_PEG_LEVEL] = 0;
  _params[VOICE_PARAM_FEG_LEVEL] = 0;

  _osc = osc;
  _noise = noise;
  _smpl = smpl;
  _oscMix = oscMix;
  _filterMix = filterMix;
  _filter = filter;
  _fegLevel = fegLevel;
  _pegLevel = pegLevel;

  _triggerSample = false;

  _osc->begin(WAVEFORM_SAWTOOTH);
  _osc->amplitude(1.0);

  _noise->amplitude(1.0);

  for (int i = 0; i < VOICE_PARAMS_LENGTH; i++) {
    SetParameter(i, _params[i]);
  }
}

void SynthDrum::SetParameter(uint8_t param, int value) {
  Voice::SetParameter(param, value);

  if (param == VOICE_PARAM_PITCH) {

    float v = m_to_f(map(value, 0, 64, 24, 96));
    _osc->frequency(v);

  } else if (param == VOICE_PARAM_CUTOFF) {

    int v = map(value, 0, 64, 400, 5000);
    _filter->frequency(v);

  } else if (param == VOICE_PARAM_RESONANCE) {

    int v = map(value, 0, 64, 0.7, 4.0); // 5 is max
    _filter->resonance(v);

  } else if (param == VOICE_PARAM_FEG_LEVEL) {

    _fegLevel->gain(value / 64.0);

  } else if (param == VOICE_PARAM_PEG_LEVEL) {

    _pegLevel->gain(value / 64.0);

  } else if (param == VOICE_PARAM_OSC_WAVE) {
    int v = map(value, 0, 64, 0, 6);

    if (v == 0) { // SAWTOOTH
      setOscMixChannel(0);
      _osc->begin(WAVEFORM_SAWTOOTH);
    } else if (v == 1) { // SQUARE
      setOscMixChannel(0);
      _osc->begin(WAVEFORM_SQUARE);
    } else if (v == 2) { // NOISE
      setOscMixChannel(1);
    } else if (v == 3) { // SAMPLE
      _triggerSample = true;
      setOscMixChannel(2);
    } else if (v == 4) { // LINE IN
      setOscMixChannel(3);
    }

  } else if (param == VOICE_PARAM_FILTER_MODE) {
    int v = map(value, 0, 64, 0, 2);
    setFilterMixChannel(v);
  }


  /*for (int i = 0; i < VOICE_PARAMS_LENGTH; i++) {
    if (i > 0) {
      Serial.print(" ");
    }
    Serial.print(paramsLabels[i]);
    Serial.print(":");
    Serial.print(_params[i]);
  }
  Serial.println();*/
}

void SynthDrum::Trigger() {
  if (_triggerSample) {
    _smpl->play(_sampleName);
  }
  Voice::Trigger();
}

void SynthDrum::SetSampleName(const char *name) {
  _sampleName = name;
}

void SynthDrum::setOscMixChannel(uint8_t channel) {
  _oscMix->gain(0, channel == 0 ? 1.0 : 0.0);
  _oscMix->gain(1, channel == 1 ? 1.0 : 0.0);
  _oscMix->gain(2, channel == 2 ? 1.0 : 0.0);
  _oscMix->gain(3, channel == 3 ? _params[VOICE_PARAM_AUDIO_IN] : 0.0);
}

void SynthDrum::setFilterMixChannel(uint8_t channel) {
  _filterMix->gain(0, channel == 0 ? 1.0 : 0.0);
  _filterMix->gain(1, channel == 1 ? 1.0 : 0.0);
  _filterMix->gain(2, 0.0);
  _filterMix->gain(3, 0.0);
}
