#include "voice.h"

Voice::Voice(Ad *env, AudioMixer4 *out, int channel) {
  for (int i = 0; i < VOICE_PARAMS_LENGTH; i++) {
    _params[i] = 0;
  }

  _env = env;
  _out = out;
  _channel = channel;
}

void Voice::Init() {
  for (int i = 0; i < VOICE_PARAMS_LENGTH; i++) {
    SetParameter(i, _params[i]);
  }
}

void Voice::Trigger() {
  _env->noteOn();
}

void Voice::SetParameter(uint8_t param, int value) {
  if (param > VOICE_PARAMS_LENGTH) {
    return;
  }

  _params[param] = value;

  if (param == VOICE_PARAM_ATTACK) {
    int v = map(value, 0, 64, 10, 2000);
    _env->attack(v);
  } else if (param == VOICE_PARAM_DECAY) {
    int v = map(value, 0, 64, 10, 2000);
    _env->release(v);
  } else if (param == VOICE_PARAM_VOLUME) {
    float v = value / 64.0;
    _out->gain(_channel, v);
  }
}

int Voice::GetParameter(uint8_t param) {
  if (param > VOICE_PARAMS_LENGTH) {
    return -1;
  }

  return _params[param];
}

void Voice::SetSampleName(const char *name) {
  //virtual method
}

bool Voice::IsActive() {
  return _env->isActive();
}

void Voice::SerialDump() {
  const char *format = "{%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d}";
  char buffer[VOICE_PARAMS_LENGTH*3];

  sprintf(buffer, format,
    _params[VOICE_PARAM_PITCH],
    _params[VOICE_PARAM_CUTOFF],
    _params[VOICE_PARAM_RESONANCE],
    _params[VOICE_PARAM_FEG_LEVEL],
    _params[VOICE_PARAM_PEG_LEVEL],
    _params[VOICE_PARAM_ATTACK],
    _params[VOICE_PARAM_DECAY],
    _params[VOICE_PARAM_OSC_WAVE],
    _params[VOICE_PARAM_VOLUME],
    _params[VOICE_PARAM_FILTER_MODE],
    _params[VOICE_PARAM_AUDIO_IN]);

  Serial.println(buffer);
}
