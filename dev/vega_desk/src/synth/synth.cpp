#include "synth.h"

//AudioInputAnalog         adc1;


// GUItool: begin automatically generated code
AudioSynthWaveformDc     dc1;            //xy=100.72222137451172,519.666669845581
//AudioInputAnalog         adc1;           //xy=103.08333206176758,464.9166555404663
Ad      eg1;      //xy=315.3372993469238,183.5992250442505
AudioAmplifier           peg1;           //xy=317.49999237060547,34.21033477783203
Ad      eg2;      //xy=318.3333168029785,414.9999542236328
AudioAmplifier           peg4;           //xy=318.3333435058594,691.6666202545166
AudioAmplifier           peg3;           //xy=319.99999237060547,471.66661262512207
AudioAmplifier           peg2;           //xy=321.6666564941406,258.3333349227905
Ad      eg3;      //xy=326.6666793823242,630.0000247955322
Ad      eg4;      //xy=334.99998474121094,866.6666603088379
AudioSynthNoiseWhite     noise1;         //xy=445.1428871154785,81.2896957397461
AudioSynthWaveformModulated osc1;   //xy=447.1984062194824,40
AudioPlaySdWav           smpl1;     //xy=448.043643951416,123.21428680419922
AudioPlaySdWav           smpl4;     //xy=448,779.6666660308838
AudioSynthNoiseWhite     noise4;         //xy=448.3333396911621,738.3333501815796
AudioSynthWaveformModulated osc4;   //xy=450.0000305175781,698.3333702087402
AudioSynthNoiseWhite     noise3;         //xy=452.6666564941406,516.6666450500488
AudioPlaySdWav           smpl3;     //xy=452.6666564941406,555.6666450500488
AudioPlaySdWav           smpl2;     //xy=456.9999656677246,348.33332920074463
AudioSynthWaveformModulated osc3;   //xy=456.66667556762695,478.33336448669434
AudioSynthNoiseWhite     noise2;         //xy=458.3333168029785,308.3332920074463
AudioSynthWaveformModulated osc2;   //xy=461.66665267944336,265.000018119812
AudioAmplifier           feg4;           //xy=571.6667327880859,818.3333463668823
AudioMixer4              oscMix4;         //xy=583.3333282470703,735.0000152587891
AudioMixer4              oscMix1;         //xy=587.8810272216797,79.58731079101562
AudioMixer4              oscMix3;         //xy=588.3333778381348,513.3333606719971
AudioAmplifier           feg2;           //xy=589.9999847412109,383.3333377838135
AudioAmplifier           feg1;           //xy=591.6071815490723,148.34923362731934
AudioAmplifier           feg3;           //xy=591.6666526794434,598.3333578109741
AudioMixer4              oscMix2;         //xy=593.333366394043,311.6666440963745
AudioFilterStateVariable filter4;        //xy=718.3333129882812,741.6666812896729
AudioFilterStateVariable filter3;        //xy=725.0000267028809,518.3333625793457
AudioFilterStateVariable filter1;        //xy=727.9602966308594,86.71429061889648
AudioFilterStateVariable filter2;        //xy=731.666675567627,316.66662788391113
AudioMixer4              filterMix3;         //xy=875.0000152587891,521.6666870117188
AudioMixer4              filterMix2;         //xy=879.9999961853027,321.6666374206543
AudioMixer4              filterMix1;         //xy=882.376953125,98.21428489685059
AudioMixer4              filterMix4;        //xy=879.9999961853027,745.0000286102295
AudioEffectMultiply      multiply1;      //xy=1109.7103691101074,168.2142858505249
AudioEffectMultiply      multiply2;      //xy=1119.9998817443848,404.9999952316284
AudioEffectMultiply      multiply3;      //xy=1120.0000839233398,609.9999895095825
AudioEffectMultiply      multiply4;      //xy=1133.3333358764648,858.3333053588867
AudioMixer4              voicesMix; //xy=1312.2381172180176,307.1428642272949
AudioEffectBitcrusher    bitcrusher1;    //xy=1445.2381172180176,275.1428642272949
AudioMixer4              fxMix1;         //xy=1590.2381172180176,314.1428642272949
AudioEffectGranular      granular1;      //xy=1721.2381172180176,280.1428642272949
AudioMixer4              fxMix2;         //xy=1877.2381172180176,322.1428642272949
AudioOutputAnalog        dac1; //xy=2003.738136291504,321.39287185668945
AudioConnection          patchCord1(dc1, eg1);
AudioConnection          patchCord2(dc1, eg2);
AudioConnection          patchCord3(dc1, eg3);
AudioConnection          patchCord4(dc1, eg4);
//AudioConnection          patchCord5(adc1, 0, oscMix1, 3);
//AudioConnection          patchCord6(adc1, 0, oscMix2, 3);
//AudioConnection          patchCord7(adc1, 0, oscMix3, 3);
//AudioConnection          patchCord8(adc1, 0, oscMix4, 3);
AudioConnection          patchCord9(eg1, peg1);
AudioConnection          patchCord10(eg1, feg1);
AudioConnection          patchCord11(eg1, 0, multiply1, 1);
AudioConnection          patchCord12(peg1, 0, osc1, 0);
AudioConnection          patchCord13(eg2, peg2);
AudioConnection          patchCord14(eg2, feg2);
AudioConnection          patchCord15(eg2, 0, multiply2, 1);
AudioConnection          patchCord16(peg4, 0, osc4, 0);
AudioConnection          patchCord17(peg3, 0, osc3, 0);
AudioConnection          patchCord18(peg2, 0, osc2, 0);
AudioConnection          patchCord19(eg3, peg3);
AudioConnection          patchCord20(eg3, feg3);
AudioConnection          patchCord21(eg3, 0, multiply3, 1);
AudioConnection          patchCord22(eg4, peg4);
AudioConnection          patchCord23(eg4, feg4);
AudioConnection          patchCord24(eg4, 0, multiply4, 1);
AudioConnection          patchCord25(noise1, 0, oscMix1, 1);
AudioConnection          patchCord26(osc1, 0, oscMix1, 0);
AudioConnection          patchCord27(smpl1, 0, oscMix1, 2);
AudioConnection          patchCord28(smpl4, 0, oscMix4, 2);
AudioConnection          patchCord29(noise4, 0, oscMix4, 1);
AudioConnection          patchCord30(osc4, 0, oscMix4, 0);
AudioConnection          patchCord31(noise3, 0, oscMix3, 1);
AudioConnection          patchCord32(smpl3, 0, oscMix3, 2);
AudioConnection          patchCord33(smpl2, 0, oscMix2, 2);
AudioConnection          patchCord34(osc3, 0, oscMix3, 0);
AudioConnection          patchCord35(noise2, 0, oscMix2, 1);
AudioConnection          patchCord36(osc2, 0, oscMix2, 0);
AudioConnection          patchCord37(feg4, 0, filter4, 1);
AudioConnection          patchCord38(oscMix4, 0, filter4, 0);
AudioConnection          patchCord39(oscMix1, 0, filter1, 0);
AudioConnection          patchCord40(oscMix3, 0, filter3, 0);
AudioConnection          patchCord41(feg2, 0, filter2, 1);
AudioConnection          patchCord42(feg1, 0, filter1, 1);
AudioConnection          patchCord43(feg3, 0, filter3, 1);
AudioConnection          patchCord44(oscMix2, 0, filter2, 0);
AudioConnection          patchCord45(filter4, 0, filterMix4, 0);
AudioConnection          patchCord46(filter4, 2, filterMix4, 1);
AudioConnection          patchCord47(filter3, 0, filterMix3, 0);
AudioConnection          patchCord48(filter3, 2, filterMix3, 1);
AudioConnection          patchCord49(filter1, 0, filterMix1, 0);
AudioConnection          patchCord50(filter1, 2, filterMix1, 1);
AudioConnection          patchCord51(filter2, 0, filterMix2, 0);
AudioConnection          patchCord52(filter2, 2, filterMix2, 1);
AudioConnection          patchCord53(filterMix3, 0, multiply3, 0);
AudioConnection          patchCord54(filterMix2, 0, multiply2, 0);
AudioConnection          patchCord55(filterMix1, 0, multiply1, 0);
AudioConnection          patchCord56(filterMix4, 0, multiply4, 0);
AudioConnection          patchCord57(multiply1, 0, voicesMix, 0);
AudioConnection          patchCord58(multiply2, 0, voicesMix, 1);
AudioConnection          patchCord59(multiply3, 0, voicesMix, 2);
AudioConnection          patchCord60(multiply4, 0, voicesMix, 3);
AudioConnection          patchCord61(voicesMix, bitcrusher1);
AudioConnection          patchCord62(voicesMix, 0, fxMix1, 1);
AudioConnection          patchCord63(bitcrusher1, 0, fxMix1, 0);
AudioConnection          patchCord64(fxMix1, granular1);
AudioConnection          patchCord65(fxMix1, 0, fxMix2, 1);
AudioConnection          patchCord66(granular1, 0, fxMix2, 0);
AudioConnection          patchCord67(fxMix2, dac1);
// GUItool: end automatically generated code

Synth::Synth() {

}

void Synth::Init() {
  for(int voice = 0; voice < 4; voice++) {
    _voices[voice] = NULL;
  }

  dac1.analogReference(INTERNAL);

  _fxDepths[0] = 0;
  _fxDepths[1] = 0;

  _fxParams[0] = 16;
  _fxParams[1] = 16;

  //fx1
  bitcrusher1.bits(8);
  SetFxDepth(0, 0);

  //fx2
  int16_t grains[12800]; //(290 ms at 44.1)
  granular1.begin(grains, 12800);
  granular1.setSpeed(_fxParams[1] / 64.);
  SetFxDepth(1, 0);

  dc1.amplitude(1.0);

  _voices[0] = new SynthDrum(&eg1, &voicesMix, 0,
    &osc1,
    &noise1,
    &smpl1,
    &oscMix1,
    &filterMix1,
    &filter1,
    &feg1,
    &peg1
  );

  _voices[0]->SetSampleName("BD1.WAV");

  _voices[1] = new SynthDrum(&eg2, &voicesMix, 1,
    &osc2,
    &noise2,
    &smpl2,
    &oscMix2,
    &filterMix2,
    &filter2,
    &feg2,
    &peg2
  );

  _voices[1]->SetSampleName("SN1.WAV");

  _voices[2] = new SynthDrum(&eg3, &voicesMix, 2,
    &osc3,
    &noise3,
    &smpl3,
    &oscMix3,
    &filterMix3,
    &filter3,
    &feg3,
    &peg3
  );

  _voices[2]->SetSampleName("HH1.WAV");

  _voices[3] = new SynthDrum(&eg4, &voicesMix, 3,
    &osc4,
    &noise4,
    &smpl4,
    &oscMix4,
    &filterMix4,
    &filter4,
    &feg4,
    &peg4
  );

  _voices[3]->SetSampleName("PC1.WAV");

  for(int voice = 0; voice < 4; voice++) {
    if (voice == 0) {
      for (int param = 0; param < VOICE_PARAMS_LENGTH; param++) {
        _voices[voice]->SetParameter(param, preset0[param]);
      }
    } else if (voice == 1) {
      for (int param = 0; param < VOICE_PARAMS_LENGTH; param++) {
        _voices[voice]->SetParameter(param, preset1[param]);
      }
    } else if (voice == 2) {
      for (int param = 0; param < VOICE_PARAMS_LENGTH; param++) {
        _voices[voice]->SetParameter(param, preset2[param]);
      }
    } else if (voice == 3) {
      for (int param = 0; param < VOICE_PARAMS_LENGTH; param++) {
        _voices[voice]->SetParameter(param, preset3[param]);
      }
    }
  }
}

Voice *Synth::GetVoice(int num) {
  return _voices[num % 4];
}

void Synth::SetFxDepth(int fx, int value) {
  _fxDepths[fx] = value;
  float maxGain = 0.3;
  float gain = value / 64.0 * maxGain;

  if (fx == 0) {
    fxMix1.gain(0, gain);
    fxMix1.gain(1, maxGain - gain);
  } else if (fx == 1) {
    fxMix2.gain(0, gain);
    fxMix2.gain(1, maxGain - gain);
  }
}

void Synth::SetFxParam(int fx, int value) {
  _fxParams[fx] = value;

  if (fx == 0) {
    int v = map(value, 0, 64, 11000, 0);
    bitcrusher1.bits(8);
    bitcrusher1.sampleRate(v);
  } else if (fx == 1) {
    granular1.setSpeed(map(value, 0, 64, 8., .5));
    granular1.beginPitchShift(map(value, 0, 64, 1., 86.));
  }
}

int Synth::GetFxDepth(int fx) {
  return _fxDepths[fx];
}

int Synth::GetFxParam(int fx) {
  return _fxParams[fx];
}

void Synth::EEpromStore() {
  for(int voice = 0; voice < 4; voice++) {
    Serial.print("int preset");
    Serial.print(voice);
    Serial.print("[VOICE_PARAMS_LENGTH]");
    Serial.print(" = ");
    _voices[voice]->SerialDump();
    /*for (int param = 0; param < VOICE_PARAMS_LENGTH; param++) {
      eeprom_save(voice * param, _voices[voice]->GetParameter(param));
    }*/
  }
}

void Synth::EEpromLoad() {
  for(int voice = 0; voice < 4; voice++) {
    /*for (int param = 0; param < VOICE_PARAMS_LENGTH; param++) {
      _voices[voice]->SetParameter(param, eeprom_load(voice * param));
    }*/
    Serial.print("STORE ");
    Serial.print(voice);
    Serial.print(": ");
    _voices[voice]->SerialDump();
  }
}
