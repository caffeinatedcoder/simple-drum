#ifndef __SYNTH_CONFIG__
#define __SYNTH_CONFIG__

#include <Arduino.h>
#include <Audio.h>
#include "voice.h"
#include "synth_drum.h"
#include "../../defines.h"

const int preset0[VOICE_PARAMS_LENGTH] = {0,0,26,0,20,0,2,16,64,0,0};
const int preset1[VOICE_PARAMS_LENGTH] = {20,64,18,0,0,0,6,20,64,0,0};
const int preset2[VOICE_PARAMS_LENGTH] = {20,64,8,0,0,0,6,20,64,20,0};
const int preset3[VOICE_PARAMS_LENGTH] = {0,64,36,0,64,0,12,0,32,0,0};

void init_synth();
Voice *init_voice(int num);

class Synth {
public:
  Synth();
  void Init();
  Voice *GetVoice(int num);
  void SetFxDepth(int fx, int value);
  void SetFxParam(int fx, int value);
  int GetFxDepth(int fx);
  int GetFxParam(int fx);
  void EEpromStore();
  void EEpromLoad();
private:
  Voice *_voices[4];
  int _fxDepths[2];
  int _fxParams[2];
};

#endif
