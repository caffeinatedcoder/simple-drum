#include "engine.h"

Engine* Engine::_instance = NULL;

Engine* Engine::Instance() {
	if(_instance == NULL){
		_instance = new Engine();
	}
	return _instance;
}

Engine::~Engine() {
	if(_instance != 0) delete _instance;
}

Engine::Engine() {
	_mainTimer = new Timer();
	_state = false;

	programSwitcher = new ProgramSwitcher();

	// board
	potA = new Pot();
	potB = new Pot();

	for (int i=0; i<BUTTONS_LEDS; i++) {
		buttonLeds[i] = new Led();
	}

	for (int i=0; i<ENCODER_LEDS; i++) {
		encoderLeds[i] = new Led();
	}

	for (int i=0; i<BUTTONS; i++) {
		buttons[i] = new Button();
	}

	for (int i=0; i<GATES; i++) {
		gates[i] = new Gate();
	}
}

void Engine::Setup() {
	_mainTimer->Setup();

	potA->Setup(ANALOG_POT_PIN_A, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);
  potB->Setup(ANALOG_POT_PIN_B, false, MIN_ANALOG_VALUE, MAX_ANALOG_VALUE);

	buttonLeds[0]->Setup(LED_BTN_A_PIN, true);
	buttonLeds[1]->Setup(LED_BTN_B_PIN, true);
	buttonLeds[2]->Setup(LED_BTN_C_PIN, true);
	buttonLeds[3]->Setup(LED_BTN_D_PIN, true);
	encoderLeds[0]->Setup(LED_ENC_A_PIN, true);
	encoderLeds[1]->Setup(LED_ENC_B_PIN, true);

	/*pinMode(BUTTON_A_PIN, INPUT_PULLUP);
	pinMode(BUTTON_B_PIN, INPUT_PULLUP);
	pinMode(BUTTON_C_PIN, INPUT_PULLUP);
	pinMode(BUTTON_D_PIN, INPUT_PULLUP);
	pinMode(BUTTON_E_PIN, INPUT_PULLUP);

	buttons[0]->attach(BUTTON_A_PIN);
	buttons[1]->attach(BUTTON_B_PIN);
	buttons[2]->attach(BUTTON_C_PIN);
	buttons[3]->attach(BUTTON_D_PIN);
	buttons[4]->attach(BUTTON_E_PIN);*/

	buttons[0]->Setup(BUTTON_A_PIN, buttonAInterruptCallback);
	buttons[1]->Setup(BUTTON_B_PIN, buttonBInterruptCallback);
	buttons[2]->Setup(BUTTON_C_PIN, buttonCInterruptCallback);
	buttons[3]->Setup(BUTTON_D_PIN, buttonDInterruptCallback);
	buttons[4]->Setup(BUTTON_E_PIN, buttonEInterruptCallback);

	gates[0]->Setup(GATE_A_IN_PIN, INPUT_PULLUP, gateInAInterruptCallback);
	gates[1]->Setup(GATE_B_IN_PIN, INPUT_PULLUP, gateInBInterruptCallback);
	gates[2]->Setup(GATE_C_IN_PIN, INPUT_PULLUP, gateInCInterruptCallback);
	gates[3]->Setup(GATE_D_IN_PIN, INPUT_PULLUP, gateInDInterruptCallback);
	gates[4]->Setup(GATE_E_IN_PIN, INPUT_PULLUP, gateInEInterruptCallback);
}

void Engine::Loop() {
	// execute program not constrained to FPS
	if (programSwitcher->GetCurrentProgram() != NULL) {
		programSwitcher->GetCurrentProgram()->Run();
	}

  unsigned long elapsed = _mainTimer->GetElapsed();

  if (elapsed >= FPS) {
    _mainTimer->Update();

		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Reset();
		}

		for (int i=0; i<ENCODER_LEDS; i++) {
			encoderLeds[i]->Reset();
		}

		// execute program
		if (programSwitcher->GetCurrentProgram() != NULL) {
			programSwitcher->GetCurrentProgram()->Loop();
		}

		// board
		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Show();
		}

		for (int i=0; i<ENCODER_LEDS; i++) {
			encoderLeds[i]->Show();
		}
  }
}

void Engine::gateInAInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(0);
}

void Engine::gateInBInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(1);
}

void Engine::gateInCInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(2);
}

void Engine::gateInDInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(3);
}

void Engine::gateInEInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(4);
}

void Engine::buttonAInterruptCallback() {
	int btn = 0;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonBInterruptCallback() {
	int btn = 1;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonCInterruptCallback() {
	int btn = 2;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonDInterruptCallback() {
	int btn = 3;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonEInterruptCallback() {
	int btn = 4;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}
