#ifndef __FASE_LUNARE_CONSTANTS__
#define __FASE_LUNARE_CONSTANTS__

#include <Arduino.h>

typedef void (*voidFuncPtr)(void);

#define VEGA
//#define DEBUG_AUDIO_RESOURCES

// VEGA SPECIFIC
#define BUTTONS_LEDS 4
#define MODE_LEDS 2
#define ENCODER_LEDS 2
#define BUTTONS 5
#define GATES 5

// GENERAL
#define FPS 1000/30 //ms 30fps
#define ONE_SECOND 60000

// SD CARD
#define SDCARD_CS_PIN    BUILTIN_SDCARD
#define SDCARD_MOSI_PIN  11
#define SDCARD_SCK_PIN   13

// ANALOG POTS
#define EMA_ALPHA 0.2
#define MIN_ANALOG_VALUE 0
#define MAX_ANALOG_VALUE 1024

// ENCODER SPECIFIC
#define ENCODER_A_A_PIN 15
#define ENCODER_A_B_PIN 14
#define ENCODER_B_A_PIN 9
#define ENCODER_B_B_PIN 8

#define ENCODER_A_BUTTON_PIN 38
#define ENCODER_B_BUTTON_PIN 37

// BUTTONS
#define BUTTON_A_PIN 27
#define BUTTON_B_PIN 26
#define BUTTON_C_PIN 25
#define BUTTON_D_PIN 24
#define BUTTON_E_PIN 39

// LEDS
#define LED_BTN_A_PIN 5
#define LED_BTN_B_PIN 2
#define LED_BTN_C_PIN 3
#define LED_BTN_D_PIN 4
#define LED_BTN_E_PIN 6

#define LED_MODE_A_PIN 21
#define LED_MODE_B_PIN 23
#define LED_MODE_C_PIN 30
#define LED_MODE_D_PIN 29

#define LED_ENC_A_PIN 36
#define LED_ENC_B_PIN 35

// CV/GATE PINS
#define GATE_A_IN_PIN 32
#define GATE_B_IN_PIN 31
#define GATE_C_IN_PIN 20
#define GATE_D_IN_PIN 22
#define GATE_E_IN_PIN 33

//  AUDIO OUTPUT
#define AUDIOOUT1   A21
//  AUDIO IN VIA RANDOM INPUT
#define AUDIOCRTL1  16

//  AUDIO OUTPUT
#define AUDIOOUT1   A21
//  AUDIO IN VIA RANDOM INPUT
#define AUDIOCRTL1  16
//  ON PCB SD CARD SLOT
#define SDCS1   10
#define SDIN    12
#define SDOUT   11
#define SCK0    13
//  FOBOE HARDWARE ON J4( NOT CONNECTED TO VEGA)
//  I2C BUS
#define SCL         19
#define SDA         18
//  SPI0 BUS & CS PIN
#define MOSI0       11
#define MISO0       12
#define SCK0        13
#define CSU3        7  // CHIP SELECT FOR W25Q128JVSIM
#define CSU4        28 // CHIP SELECT FOR 23LC1024-I_SN
//  SERIAL BUS
#define TX1         1
#define RX1         0
//  FOBOE HARDWARE ON J1 CONNECTOR NOT USED ON VEGA
#define DPLUS       A25
#define DMINUS      A26
#define AUDIOOUT2   A22
#define AREF1       AREF
#define A11         A11
#define A10         A10
#define A3          17
#define A15         34
#define HOST_5V     5V
//#define SCL           19 ALREADY DECLARED. ONLY FOR REMINDER
//#define SDA           18 ALREADY DECLARED. ONLY FOR REMINDER

#endif
