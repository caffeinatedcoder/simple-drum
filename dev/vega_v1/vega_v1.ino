#include <Audio.h>
#include "src/synth/_include.h"
#include "src/engine.h"
#include "src/programs/_include.h"

#include "src/utils/_include.h"

Engine *engine = Engine::Instance();

void setup() {
  Serial.begin(9600);

  SD.begin(SDCARD_CS_PIN);

  AudioMemory(40);
  AudioInterrupts();

  Synth *synth = new Synth();
  synth->Init();

  engine->Setup();
  engine->programSwitcher->PushProgram(new ProgramStartup());
  engine->programSwitcher->PushProgram(new MainProgram(synth));
}

void loop() {
  engine->Loop();
}
