#include "program_startup.h"

ProgramStartup::ProgramStartup() {
  Setup();
}

void ProgramStartup::Setup() {
  _timer = new Timer();
  _tic = 0;
}

void ProgramStartup::Loop() {
  Engine *engine = Engine::Instance();

  int led = _tic % 4;
  if (led == 3) {
    led = 2;
  } else if (led == 2) {
    led = 3;
  }

  engine->modeLeds[led]->Set(16);

  engine->buttonLeds[0]->Set(true);
  engine->buttonLeds[1]->Set(_tic > 5);
  engine->buttonLeds[2]->Set(_tic > 10);
  engine->buttonLeds[3]->Set(_tic > 15);

  if (_tic >= 20) {
    engine->programSwitcher->NextProgram();
  }

  _tic ++;
}
