#ifndef __PROGRAM_STARTUP__
#define __PROGRAM_STARTUP__

#include <Arduino.h>
#include "../engine.h"
#include "../core/_include.h"
#include "../utils/_include.h"

class ProgramStartup : public Program {
public:
  ProgramStartup();
  void Setup();
  void Loop();
private:
  Timer *_timer;
  int _tic;
};

#endif
