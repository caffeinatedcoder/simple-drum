#ifndef __PROGRAM_MAIN__
#define __PROGRAM_MAIN__

#include <Arduino.h>
#include "../engine.h"
#include "../synth/_include.h"
#include "../utils/_include.h"

const int matrix[] = {
  VOICE_PARAM_PITCH, VOICE_PARAM_PEG_LEVEL,
  VOICE_PARAM_CUTOFF, VOICE_PARAM_FEG_LEVEL,
  VOICE_PARAM_ATTACK, VOICE_PARAM_DECAY,
  VOICE_PARAM_RESONANCE, VOICE_PARAM_FILTER_MODE,
  VOICE_PARAM_VOLUME, VOICE_PARAM_OSC_WAVE
};

class MainProgram : public Program {
public:
  MainProgram(Synth *synth);
  void Setup();
  void Loop();
  void Run();
  void OnGateIn(uint8_t channel);
  void OnKeyDown(int key);
private:
  Synth *_synth;
  Timer *_timer;

  // selected voice
  int _selectedIndex;
  int _selectedFx;

  // matrix params
  int _page;

  // params lock
  bool _aLock;
  bool _bLock;
  int _aMem;
  int _bMem;

  // system
  int _cycle;
  bool _blink;

  bool _ledStates[4];

  void processTriggers();
  void processButtons();
  void updateParams();
  void updateLeds();

  int getSelectedAParam();
  void setSelectedAParam(int value);

  int getSelectedBParam();
  void setSelectedBParam(int value);
};

#endif
