#include "program_switcher.h"

ProgramSwitcher::ProgramSwitcher() {
  _current = 0;
  _root = NULL;
}

void ProgramSwitcher::PushProgram(Program *program) {
  if (_root == NULL) {
    _root = program;
    return;
  }

  Program *current = _root;
  while (current->GetNext() != NULL) {
      current = current->GetNext();
  }
  current->SetNext(program);
}

void ProgramSwitcher::NextProgram() {
  _current = (_current + 1) % CountPrograms();
  GetCurrentProgram()->Reset();
}

void ProgramSwitcher::PrevProgram() {
  _current = (_current - 1) % CountPrograms();
  GetCurrentProgram()->Reset();
}

void ProgramSwitcher::GoToProgram(int index) {
  _current = index % CountPrograms();
  GetCurrentProgram()->Reset();
}

Program *ProgramSwitcher::GetCurrentProgram() {
  return getNth(_current);
}

int ProgramSwitcher::CountPrograms() {
  int count = 0;
  Program *current = _root;
  while (current != NULL) {
    current = current->GetNext();
    count++;
  }
  return count;
}

Program *ProgramSwitcher::getNth(int index) {
  int count = 0;
  Program *found = NULL;
  Program *current = _root;
  while(current != NULL) {
    if (count == index) {
      found = current;
      break;
    }
    count++;
    current = current->GetNext();
  }

  return found;
}

int ProgramSwitcher::CurrentProgramIndex() {
  return _current;
}
