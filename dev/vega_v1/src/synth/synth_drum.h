#ifndef __SYNTH_DRUM__
#define __SYNTH_DRUM__

#include <Audio.h>
#include "voice.h"

class SynthDrum : public Voice {
public:
  SynthDrum(Ad *env, AudioMixer4 *out, int channel,
    AudioSynthWaveformModulated *osc,
    AudioSynthNoiseWhite *noise,
    AudioPlaySdWav *smpl,
    AudioMixer4 *oscMix,
    AudioMixer4 *filterMix,
    AudioFilterStateVariable *filter,
    AudioAmplifier *fegLevel,
    AudioAmplifier *pegLevel
  );
  void SetFrequency(int value);
  void SetParameter(uint8_t param, int value);
  void SetSampleName(const char *name);
  void Trigger();
private:
  AudioSynthWaveformModulated *_osc;
  AudioSynthNoiseWhite *_noise;
  AudioPlaySdWav *_smpl;
  AudioMixer4 *_oscMix;
  AudioMixer4 *_filterMix;
  AudioFilterStateVariable *_filter;
  AudioAmplifier *_fegLevel;
  AudioAmplifier *_pegLevel;

  const char *_sampleName;
  bool _triggerSample;

  void setOscMixChannel(uint8_t channel);
  void setFilterMixChannel(uint8_t channel);
};

#endif
