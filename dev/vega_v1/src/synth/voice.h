#ifndef __SYNTH_VOICE__
#define __SYNTH_VOICE__

#include <Arduino.h>
#include <Audio.h>
#include "../audio/_include.h"
#include "../utils/_include.h"

#define VOICE_PARAM_PITCH 0
#define VOICE_PARAM_CUTOFF 1
#define VOICE_PARAM_RESONANCE 2
#define VOICE_PARAM_FEG_LEVEL 3
#define VOICE_PARAM_PEG_LEVEL 4
#define VOICE_PARAM_ATTACK 5
#define VOICE_PARAM_DECAY 6
#define VOICE_PARAM_OSC_WAVE 7
#define VOICE_PARAM_VOLUME 8
#define VOICE_PARAM_FILTER_MODE 9
#define VOICE_PARAM_AUDIO_IN 10
// #define VOICE_PARAM_SAMPLE_NAME 11
#define VOICE_PARAMS_LENGTH 11

class Voice {
public:
  Voice(Ad *env, AudioMixer4 *out, int channel);
  void Init();
  virtual void Trigger();
  virtual void SetParameter(uint8_t param, int value);
  int GetParameter(uint8_t param);
  virtual void SetSampleName(const char *name);
  bool IsActive();
  void SerialDump();
protected:
  AudioMixer4 *_out;
  Ad *_env;
  int _channel;

  int _params[VOICE_PARAMS_LENGTH];
};

#endif
