#include "engine.h"

Engine* Engine::_instance = NULL;

Engine* Engine::Instance() {
	if(_instance == NULL){
		_instance = new Engine();
	}
	return _instance;
}

Engine::~Engine() {
	if(_instance != 0) delete _instance;
}

Engine::Engine() {
	_mainTimer = new Timer();
	_state = false;

	programSwitcher = new ProgramSwitcher();

	// board
	for (int i=0; i<BUTTONS_LEDS; i++) {
		buttonLeds[i] = new Led();
	}

	for (int i=0; i<MODE_LEDS; i++) {
		modeLeds[i] = new Led();
	}

	for (int i=0; i<ENCODER_LEDS; i++) {
		encoderLeds[i] = new Led();
	}

	for (int i=0; i<BUTTONS; i++) {
		buttons[i] = new Button();
	}

	for (int i=0; i<GATES; i++) {
		gates[i] = new Gate();
	}

	encoderA = new Encoder();
	encoderB = new Encoder();
}

void Engine::Setup() {
	_mainTimer->Setup();

	buttonLeds[0]->Setup(LED_BTN_A_PIN, true);
	buttonLeds[1]->Setup(LED_BTN_B_PIN, true);
	buttonLeds[2]->Setup(LED_BTN_C_PIN, true);
	buttonLeds[3]->Setup(LED_BTN_D_PIN, true);
	buttonLeds[4]->Setup(LED_BTN_E_PIN, true);

	modeLeds[0]->Setup(LED_MODE_A_PIN);
	modeLeds[1]->Setup(LED_MODE_B_PIN);
	modeLeds[2]->Setup(LED_MODE_C_PIN);
	modeLeds[3]->Setup(LED_MODE_D_PIN);

	encoderLeds[0]->Setup(LED_ENC_A_PIN);
	encoderLeds[1]->Setup(LED_ENC_B_PIN);

	encoderA->Setup(ENCODER_A_B_PIN, ENCODER_A_A_PIN, encoderAInterruptCallback);
	encoderB->Setup(ENCODER_B_B_PIN, ENCODER_B_A_PIN, encoderBInterruptCallback);

	gates[0]->Setup(GATE_A_IN_PIN, INPUT_PULLUP, gateInAInterruptCallback);
	gates[1]->Setup(GATE_B_IN_PIN, INPUT_PULLUP, gateInBInterruptCallback);
	gates[2]->Setup(GATE_C_IN_PIN, INPUT_PULLUP, gateInCInterruptCallback);
	gates[3]->Setup(GATE_D_IN_PIN, INPUT_PULLUP, gateInDInterruptCallback);
	gates[4]->Setup(GATE_E_IN_PIN, INPUT_PULLUP, gateInEInterruptCallback);

	buttons[0]->Setup(BUTTON_A_PIN, buttonAInterruptCallback);
	buttons[1]->Setup(BUTTON_B_PIN, buttonBInterruptCallback);
	buttons[2]->Setup(BUTTON_C_PIN, buttonCInterruptCallback);
	buttons[3]->Setup(BUTTON_D_PIN, buttonDInterruptCallback);
	buttons[4]->Setup(BUTTON_E_PIN, buttonEInterruptCallback);
}

void Engine::Loop() {
	// execute program not constrained to FPS
	if (programSwitcher->GetCurrentProgram() != NULL) {
		programSwitcher->GetCurrentProgram()->Run();
	}

  unsigned long elapsed = _mainTimer->GetElapsed();

  if (elapsed >= FPS) {
    _mainTimer->Update();

		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Reset();
		}

		for (int i=0; i<ENCODER_LEDS; i++) {
			encoderLeds[i]->Reset();
		}

		for (int i=0; i<MODE_LEDS; i++) {
			modeLeds[i]->Reset();
		}

		// execute program
		if (programSwitcher->GetCurrentProgram() != NULL) {
			programSwitcher->GetCurrentProgram()->Loop();
		}

		// board
		for (int i=0; i<BUTTONS_LEDS; i++) {
			buttonLeds[i]->Show();
		}

		for (int i=0; i<MODE_LEDS; i++) {
			modeLeds[i]->Show();
		}

		for (int i=0; i<ENCODER_LEDS; i++) {
			encoderLeds[i]->Show();
		}
  }
}

void Engine::encoderAInterruptCallback() {
	Engine::Instance()->encoderA->InterruptCallback();
}

void Engine::encoderBInterruptCallback() {
	Engine::Instance()->encoderB->InterruptCallback();
}

void Engine::gateInAInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(0);
}

void Engine::gateInBInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(1);
}

void Engine::gateInCInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(2);
}

void Engine::gateInDInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(3);
}

void Engine::gateInEInterruptCallback() {
	Engine::Instance()->programSwitcher->GetCurrentProgram()->OnGateIn(4);
}

void Engine::buttonAInterruptCallback() {
	int btn = 0;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonBInterruptCallback() {
	int btn = 1;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonCInterruptCallback() {
	int btn = 2;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonDInterruptCallback() {
	int btn = 3;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}

void Engine::buttonEInterruptCallback() {
	int btn = 4;
	if (Engine::Instance()->buttons[btn]->Dispatch()) {
		Engine::Instance()->programSwitcher->GetCurrentProgram()->OnKeyDown(btn);
	}
}
