#ifndef __PROGRAM_TEST__
#define __PROGRAM_TEST__

#include <Arduino.h>
#include "../engine.h"
#include "../faselunare/core/_include.h"
#include "../synth/_include.h"

#include <Audio.h>

class ProgramTest : public Program {
public:
  ProgramTest();
  void Setup();
  void Loop();
private:
  void setupAudio();
  void processTriggers(int *leds);
  void processButtons();
  void updateParams();
  void updateLeds(int *leds);

  int getSelectedAParam();
  void setSelectedAParam(int value);

  int getSelectedBParam();
  void setSelectedBParam(int value);

  void setFxDepth(int value);
  void setFxParam(int value);

  Part *_parts[4];

  // selected part
  int _selectedIndex;
  Part *_selected;
  int _selectedFx;

  // matrix params
  int _mode;

  // params lock
  bool _aLock;
  bool _bLock;
  int _aMem;
  int _bMem;

  // system
  int _cycle;
  bool _blink;

  Ad envelope1;
};

#endif
