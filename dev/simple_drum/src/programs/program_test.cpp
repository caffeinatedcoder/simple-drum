#include "program_test.h"

ProgramTest::ProgramTest() {
  Setup();
}

void ProgramTest::Setup() {
  Serial.begin(9600);

  setupAudio();

  // selected part
  _selectedIndex = 0;
  _selectedFx = 0;

  // matrix params
  _mode = 0;

  // params lock
  _aLock = true;
  _bLock = true;
  _aMem = getSelectedAParam();
  _bMem = getSelectedBParam();

  // system
  _cycle = 0;
  _blink = true;
}

void ProgramTest::Loop() {
  int leds[] = {0,0,0,0};

  processTriggers(leds);
  //processButtons();
  //updateParams();
  updateLeds(leds);

  _cycle = (_cycle + 1) % 64;
  _blink = (_cycle % 4);
}

void ProgramTest::setupAudio() {
  // Audio connections require memory to work.  For more
  // detailed information, see the MemoryAndCpuUsage example
  AudioMemory(40);

  // GUItool: begin automatically generated code
  AudioSynthWaveformSine   sine1;          //xy=66,36
  AudioSynthNoiseWhite     noise2;         //xy=67,156
  AudioSynthNoiseWhite     noise1;         //xy=68,102
  AudioSynthNoisePink      noise3;         //xy=68,329
  AudioSynthWaveformPWM    pwm1;           //xy=69,224
  AudioSynthWaveformPWM    pwm2;           //xy=71,276
  AudioFilterBiquad        biquad2;        //xy=211,155
  AudioFilterBiquad        biquad1;        //xy=212,102
  AudioMixer4              mixer2;         //xy=218,282
                           envelope1;      //xy=372,33 /*AudioEffectEnvelope*/
  Ad                       envelope4;      //xy=372,282
  Ad                       envelope2;      //xy=373,101
  Ad                       envelope3;      //xy=373,155
  AudioMixer4              mixer1;         //xy=552,136
  AudioEffectBitcrusher    bitcrusher1;    //xy=685,104
  AudioMixer4              mixer3;         //xy=821,141
  AudioEffectGranular      granular1;      //xy=952,107
  AudioMixer4              mixer4;         //xy=1084,147
  AudioOutputAnalog        dac1;           //xy=1214,147

  AudioConnection          patchCord1(sine1, dac1);
  //AudioConnection          patchCord20(mixer1, dac1);

  /*
  AudioConnection          patchCord1(sine1, envelope1);
  AudioConnection          patchCord2(noise2, biquad2);
  AudioConnection          patchCord3(noise1, biquad1);
  AudioConnection          patchCord4(noise3, 0, mixer2, 2);
  AudioConnection          patchCord5(pwm1, 0, mixer2, 0);
  AudioConnection          patchCord6(pwm2, 0, mixer2, 1);
  AudioConnection          patchCord7(biquad2, envelope3);
  AudioConnection          patchCord8(biquad1, envelope2);
  AudioConnection          patchCord9(mixer2, envelope4);
  AudioConnection          patchCord10(envelope1, 0, mixer1, 0);
  AudioConnection          patchCord11(envelope4, 0, mixer1, 3);
  AudioConnection          patchCord12(envelope2, 0, mixer1, 1);
  AudioConnection          patchCord13(envelope3, 0, mixer1, 2);
  AudioConnection          patchCord14(mixer1, bitcrusher1);
  AudioConnection          patchCord15(mixer1, 0, mixer3, 1);
  AudioConnection          patchCord16(bitcrusher1, 0, mixer3, 0);
  AudioConnection          patchCord17(mixer3, granular1);
  AudioConnection          patchCord18(mixer3, 0, mixer4, 1);
  AudioConnection          patchCord19(granular1, 0, mixer4, 0);
  AudioConnection          patchCord20(mixer4, dac1);
  */
  // GUItool: end automatically generated code

  // by default the Teensy 3.1 DAC uses 3.3Vp-p output
  // if your 3.3V power has noise, switching to the
  // internal 1.2V reference can give you a clean signal
  dac1.analogReference(INTERNAL);
  AudioInterrupts();

  //bd
  sine1.frequency(110);
  envelope1.attack(15);
  envelope1.release(500);
  mixer1.gain(0, 0.5);

  //fx1
  mixer3.gain(0, 0);
  mixer3.gain(1, 0.5);

  //fx2
  mixer4.gain(0, 0);
  mixer4.gain(1, 0.5);

  //_parts[0] = new Bd808(envelope1, mixer1, 0, sine1);
  //_parts[1] = new Sd808(envelope2, mixer1, 1, noise1, biquad1);
  //_parts[2] = new Hh(envelope3, mixer1, 3, noise2, biquad2);
  //_parts[3] = new Perc(envelope4, mixer1, 4, pwm1, pwm2, noise3, mixer2, biquad2);
}

void ProgramTest::processTriggers(int *leds) {
  /*Engine *engine = Engine::Instance();

  for (int i=0; i<4; i++) {
    if (engine->GetGateIn(i)->fallingEdge()) {
      _parts[i]->Trigger();
    }
  }*/

  // return;

  //simulate triggers

  int bd[] = {0, 1};
  int sd[] = {4, 7};
  int hh[] = {2, 6};
  int perc[] = {3, 5};

  int division = 8;

  for (uint i = 0; i < sizeof(bd)/sizeof(bd[0]); i++) {
    if (_cycle == bd[i]*division) {
      //_parts[0]->Trigger();
      envelope1.noteOn();
      leds[0] = 1;
    }
  }

  /*for (uint i = 0; i < sizeof(sd)/sizeof(sd[0]); i++) {
    if (_cycle == sd[i]*division) {
      _parts[1]->Trigger();
      leds[1] = 1;
    }
  }

  for (uint i = 0; i < sizeof(hh)/sizeof(hh[0]); i++) {
    if (_cycle == hh[i]*division) {
      _parts[2]->Trigger();
      leds[2] = 1;
    }
  }

  for (uint i = 0; i < sizeof(perc)/sizeof(perc[0]); i++) {
    if (_cycle == perc[i]*division) {
      _parts[3]->Trigger();
      leds[3] = 1;
    }
  }*/
}

void ProgramTest::processButtons() {
  Engine *engine = Engine::Instance();

  bool changed = false;

  if (_mode == 0 || _mode == 1) {

    for (int i=0; i < 4; i++) {
      if (engine->GetButton(i)->fell()) {
        _selectedIndex = i;
        _selected = _parts[i];
        changed = true;
        break;
      }
    }

  } else if (_mode == 2) {

    for (int i=0; i < 2; i++) {
      if (engine->GetButton(i)->fell()) {
        _selectedFx = i;
        changed = true;
        break;
      }
    }

    if (engine->GetButton(2)->fell()) {
      // TODO: STORE
      /*Serial.println("-----BEGIN-----");

      for (int i=0; i<4; i++) {

        Serial.println(_parts[i]->GetAttack());
        Serial.println(_parts[i]->GetRelease());
        Serial.println(_parts[i]->GetFrequency());
        Serial.println(_parts[i]->GetVolume());
        Serial.println("----------");
      }

      Serial.println("----------");
      Serial.println(_fx1Depth);
      Serial.println(_fx1Param);
      Serial.println("----------");
      Serial.println(_fx2Depth);
      Serial.println(_fx2Param);
      Serial.println("-----END-----");*/
    }

    if (engine->GetButton(3)->fell()) {
    }
  }

  if (engine->GetButton(4)->fell()) {
    _mode = (_mode + 1) % 3;
    changed = true;
  }

  if (changed) {
    _aMem = getSelectedAParam();
    _bMem = getSelectedBParam();
    _aLock = true;
    _bLock = true;
  }
}

void ProgramTest::updateParams() {
  Engine *engine = Engine::Instance();

  int a = engine->GetPotA()->ReadAs64();
  int b = engine->GetPotB()->ReadAs64();

  if (a == _aMem) {
    engine->GetLed(4)->Set(true);
    _aLock = false;
  }

  if (b == _bMem) {
    engine->GetLed(5)->Set(true);
    _bLock = false;
  }

  if (!_aLock) {
    setSelectedAParam(a);
  }

  if (!_bLock) {
    setSelectedBParam(b);
  }
}

void ProgramTest::updateLeds(int *leds) {
  Engine *engine = Engine::Instance();

  if (_mode == 0 || _mode == 1) {
    for (int i = 0; i < 4; i++) {
      if (i != _selectedIndex) {
        engine->GetLed(i)->Set(leds[i]);
      } else {
        engine->GetLed(i)->Set(!leds[i]);
      }
    }
  } else {
    engine->GetLed(_selectedFx)->Set(_blink);
  }

  //TODO: mode led not present on the board yet
}

int ProgramTest::getSelectedAParam() {
  if (_mode == 0) {
    return _selected->GetFrequency();
  } else if (_mode == 1) {
    return _selected->GetVolume();
  } else if (_mode == 2) {
    //return _selectedFx==0 ? _fx1Depth : _fx2Depth;
  }

  return -1;
}

int ProgramTest::getSelectedBParam() {
  if (_mode == 0) {
    return _selected->GetRelease();
  } else if (_mode == 1) {
    return _selected->GetAttack();
  } else if (_mode == 2) {
    //return _selectedFx==0 ? _fx1Param : _fx2Param;
  }

  return -1;
}

void ProgramTest::setSelectedAParam(int value) {
  if (_mode == 0) {
    _selected->SetFrequency(value);
  } else if (_mode == 1) {
    _selected->SetVolume(value);
  } else if (_mode == 2) {
    /*if (_selectedFx == 0) {
      _fx1Depth = value;
      setFxDepth(_fx1Mix, _fx1Depth);
    } else {
      _fx2Depth = value;
      setFxDepth(_fx2Mix, _fx2Depth);
    }*/
  }
}

void ProgramTest::setSelectedBParam(int value) {
  if (_mode == 0) {
    _selected->SetRelease(value);
  } else if (_mode == 1) {
    _selected->SetAttack(value);
  } else if (_mode == 2) {
    /*if (_selectedFx == 0) {
      _fx1Param = value;
      setFxParam(_fx1Param);
    } else {
      _fx2Param = value;
      setFxParam(_fx2Param);
    }*/
  }
}

void ProgramTest::setFxDepth(int value) {
  /*mix->gain(0, (value/64.));
  mix->gain(1, 1.0-(value/64.));*/
}

void ProgramTest::setFxParam(int value) {
  /*if (_selectedFx == 0) {
    _fx1Param = value;
    int v = map(_fx1Param, 0, 64, 11000, 0);
    _bitCrusher->sampleRate(v);
  } else {
    _fx2Param = value;
    _granular->setSpeed(map(_fx2Param, 0, 64, 8., .5));
    _granular->beginPitchShift(map(_fx2Param, 0, 64, 1., 86.));
  }*/
}
