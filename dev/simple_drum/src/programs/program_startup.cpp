#include "program_startup.h"

ProgramStartup::ProgramStartup() {
  Setup();
}

void ProgramStartup::Setup() {
  _timer = new Timer();
  _cycle = 0;
}

void ProgramStartup::Loop() {
  Engine *engine = Engine::Instance();

  engine->GetLed(_cycle)->Set(true);

  _cycle = (_cycle + 1) % 6;

  if (_cycle == 0) {
    engine->GetProgramSwitcher()->NextProgram();
  }
}
