#ifndef __PROGRAM_STARTUP__
#define __PROGRAM_STARTUP__

#include <Arduino.h>
#include "../engine.h"
#include "../faselunare/core/_include.h"

class ProgramStartup : public Program {
public:
  ProgramStartup();
  void Setup();
  void Loop();
private:
  Timer *_timer;
  int _cycle;
};

#endif
