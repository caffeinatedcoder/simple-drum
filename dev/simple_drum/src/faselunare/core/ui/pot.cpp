#include "pot.h"

Pot::Pot() {

}

void Pot::Setup(uint8_t pin, bool inverted) {
  _pin = pin;
  _ema = 0;
  _max = 0;
  _delta = 0;
  _inverted = inverted;

  _ema = analogRead(_pin);
  if (_inverted) {
    _ema = MAX_ANALOG_VALUE - _ema;
  }

  _max = _ema;
}

int Pot::ReadRaw() {
  int value = analogRead(_pin);
  if (_inverted) {
    value = MAX_ANALOG_VALUE - value;
  }
  _max = value > _max ? value : _max;
  return value;
}

int Pot::GetMax() {
  return _max;
}

int Pot::ReadAnalog() {
  int value = analogRead(_pin);
  if (_inverted) {
    value = MAX_ANALOG_VALUE - value;
  }

  // TODO: MUST BE OPTIMIZED (GOOD CLOCKWISE, BAD COUNTER CLOCKWISE)
  _delta = abs(value - _ema);

  _max = value > _max ? value : _max;
  _ema = exponential_moving_average(value, _ema);
  return _ema;
}

int Pot::ReadAsUnipolar() {
  return map(ReadAnalog(), 0, MAX_ANALOG_VALUE, 0, 127);
}

int Pot::ReadAsBipolar() {
  return map(ReadAnalog(), 0, MAX_ANALOG_VALUE, -64, 64);
}

int Pot::ReadAs128() {
  return ReadAsUnipolar();
}

int Pot::ReadAs64() {
  return map(ReadAnalog(), 0, MAX_ANALOG_VALUE, 0, 63);
}

int Pot::ReadAs16() {
  return map(ReadAnalog(), 0, MAX_ANALOG_VALUE, 0, 15);
}

int Pot::ReadAs8() {
  return map(ReadAnalog(), 0, MAX_ANALOG_VALUE, 0, 7);
}

int Pot::ReadAs4() {
  return map(ReadAnalog(), 0, MAX_ANALOG_VALUE, 0, 3);
}
