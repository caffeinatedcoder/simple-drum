#include "button.h"

Button::Button() {
}

void Button::Setup(uint8_t pin, uint8_t mode) {
	_pin = pin;
  _mode = mode;
	pinMode(_pin, OUTPUT);
}
