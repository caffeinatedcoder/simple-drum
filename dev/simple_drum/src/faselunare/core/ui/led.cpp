#include "led.h"

Led::Led() {
}

void Led::Setup(int pin) {
	_pin = pin;
  _value = false;
	pinMode(_pin, OUTPUT);
}

void Led::Reset() {
  _value = false;
}

void Led::Set(bool value) {
  _value = value;
}

void Led::Show() {
  digitalWrite(_pin, _value ? HIGH : LOW);
}
