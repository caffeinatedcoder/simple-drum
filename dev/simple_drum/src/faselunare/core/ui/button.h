#ifndef __FASE_LUNARE_CORE_BUTTON__
#define __FASE_LUNARE_CORE_BUTTON__

#include <Arduino.h>
#include "../defines.h"
#include "../utils/_include.h"

class Button {
public:
  Button();
  void Setup(uint8_t pin, uint8_t mode);

private:
  uint8_t _pin;
  uint8_t _mode;
};

#endif
