#include "program.h"

Program::Program() {
  _next = NULL;
}

void Program::Setup() {
}

void Program::Loop() {
}

void Program::SetNext(Program *program) {
    _next = program;
}

Program *Program::GetNext() {
  return _next;
}
