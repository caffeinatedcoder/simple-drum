#ifndef __FASE_LUNARE_CONSTANTS__
#define __FASE_LUNARE_CONSTANTS__

// #define PI 3.14159265358979323846

#define EMA_ALPHA 0.2
#define MAX_ANALOG_VALUE 1010 //830 //854

#define ONE_SECOND 60000
#define A440 440

// TCA8418 (KEYBOARD)

#define KEYBOARD_STATE_REGISTER 0x04
#define KEYBOARD_FLAG_REGISTER 0x02
#define KEYBOARD_KEY_DOWN 0x80
#define KEYBOARD_KEY_MASK 0x7F

// ?
const int KEYBOARD_CONFIG_COMMANDS[] = {
  0x1D, 0x1F, 0x1E, 0xFF, 0x1F, 0x03, 0x01, 0xB9, 0x02, 0x0F
};


#endif
