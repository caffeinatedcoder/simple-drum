#include "triggerout.h"

TriggerOut::TriggerOut() {
}

void TriggerOut::Setup(int pin) {

  _pin = pin;
  pinMode(_pin, OUTPUT);
}

void TriggerOut::Send(bool value) {
  digitalWrite(_pin, value ? HIGH : LOW);
}
