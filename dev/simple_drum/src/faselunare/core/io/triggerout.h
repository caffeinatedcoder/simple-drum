#ifndef __FASE_LUNARE_TRIGGER_OUT__
#define __FASE_LUNARE_TRIGGER_OUT__

#include <Arduino.h>

class TriggerOut {
public:
  TriggerOut();
  void Setup(int pin);
  void Send(bool value);
private:
  int _pin;
};
#endif
