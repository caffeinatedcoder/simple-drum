#ifndef __SYNTH_PERC__
#define __SYNTH_PERC__

#include <Audio.h>
#include "part.h"

class Perc : public Part {
public:
  Perc(Ad &env, AudioMixer4 &out, int channel,
    AudioSynthWaveformPWM &pwm1, AudioSynthWaveformPWM &pwm2,
    AudioSynthNoisePink &noise, AudioMixer4 &mixer, AudioFilterBiquad &filter);
  void SetFrequency(int value);
private:
  AudioSynthWaveformPWM _pwm1;
  AudioSynthWaveformPWM _pwm2;
  AudioSynthNoisePink _noise;
  AudioMixer4 _mixer;
  AudioFilterBiquad _filter;
};

#endif
