#include "sd808.h"

/*
  +-------+   +-----+
  | SINE  +-->+ ENV |
  +-------+   +-----+
*/

Sd808::Sd808(Ad &env, AudioMixer4 &out, int channel, AudioSynthNoiseWhite &noise, AudioFilterBiquad &filter) : Part(env, out, channel) {
  _volume = 64;
  _frequency = 20;
  _attack = 0;
  _release = 5;

  _noise = noise;
  _filter = filter;

  _noise.amplitude(0.2);

  Init();
}

void Sd808::SetFrequency(int value) {
  _frequency = value;
  int v = map(_frequency, 0, 64, 400, 5000);
  _filter.setLowpass(0, v, 0.5);
}
